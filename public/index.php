<?php
session_start();
require_once('../config/autoload.php');
require_once('../config/config.php');
require_once('../config/database.php');
$url = $_SERVER['REQUEST_URI'];
if ($url != '/') {
    $url = explode("/", $_SERVER['REQUEST_URI']);
    $url_sub = explode("?", $url[1]);
}

if (isset($url[1])) {
    if (!empty($url_sub[1])){
        $url[1] = $url_sub[0];
    }
    if ($url[1] != 'admin') {
        $controller = $url[1];
        if (isset($url[2])) {
            $action = $url[2];
        } else {
            $action = 'index';
        }
    } else {
        $admin = $url[1];
        if (isset($url[2])) {
            $controller = $url[2];
            if (isset($url[3])) {
                $action = $url[3];
            } else {
                $action = 'index';
            }
        }
    }
}
//
//else {
//    header('Location: admin/Login/login');
//    die;
//}
require_once('../routes/routes.php');

?>
