/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */

function Main() {
}

Main.fn = {
    init: function () {
        Main.fn.initCheckbox.call(this);
        Main.fn.initdatetimepicker.call(this);
        Main.fn.initItemActions.call(this);
        Main.fn.ininchecknumber.call(this);
        Main.fn.initPerPage.call(this);
        Main.fn.initFilter.call(this);
        Main.fn.initFilterDate.call(this);
        Main.fn.initActive.call(this);
        Main.fn.initimage.call(this);
    },

    initdatetimepicker: function () {

        $('#example').DataTable({
            "paging": false,
            "searching": false,
            "responsive": true
        });

        $('#datetimepicker').datepicker({
            viewMode: 'years'
        });


        var password = $('.password');
        password.prop('disabled', true);
        $('#changepassword').change(function () {
            if ($(this).is(":checked")) {
                $('#changepassword').val('on');
                password.removeAttr('disabled');
                password.prop('required', true);
                $("#confirm_password").prop('required', true)
            } else {
                $('#changepassword').val('off');
                $(".password").attr('disabled', '');
            }
        });
    },

    initimage: function () {
        $(document).on('change', '.btn-file :file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function (event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function () {
            readURL(this);
            $('#img-upload-remove').remove();
        });

    },

    ininchecknumber: function () {
        var el = document.getElementById("phone");

        //---------------------------------Chỉ cho nhập số---------------------------------
        if (el) {
            el.addEventListener("keypress", function (evt) {
                if (
                    (evt.which != 8 && evt.which != 0 && evt.which < 48) ||
                    evt.which > 57
                ) {
                    evt.preventDefault();
                }
            });
        }

        $(".nav-tabs a").click(function () {
            $(this).tab('show');
            lava.redrawCharts();
        });
    },

    initCheckbox: function () {
        $('#select_all').change(function () {
            var checkboxes = $(this).closest('table').find(':checkbox');

            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
                $('.table tbody tr').addClass('selected_item');
            } else {
                checkboxes.prop('checked', false);
                $('.table tbody tr').removeClass('selected_item');
            }
        });

        $('.checkbox_item').change(function () {
            var parent = $(this).parent().parent().parent().parent();

            if ($(this).is(':checked')) {
                parent.addClass('selected_item');
            } else {
                parent.removeClass('selected_item');
                $('#select_all').prop('checked', false);

            }

            var length_checkbox_item = $('.checkbox_item').length;
            var length_selected_item = $('.selected_item').length;
            if (length_checkbox_item == length_selected_item) {
                $('#select_all').prop('checked', true);
            }

        });

    },

    initItemActions: function () {
        // update status list checkbox
        $(document).on('click', '.item_actions', function () {
            var dataLink = $(this).attr('data-link');
            var link = $('#' + dataLink).val();
            var key = $(this).attr('data-key');
            var text = $(this).attr('data-text');
            var title = $(this).attr('data-title');
            var id = $(this).attr('data-id');
            $('#data-id').val(id);
            if (id && link && key) {
                swal({
                    title: "Bạn có chắc chắn " + text + " " + title + " này ?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "Yes",
                    confirmButtonClass: "btn-danger",
                    cancelButtonText: "No",
                }, function () {
                    $('body').loading('toggle');
                    $.ajax({
                        type: 'GET',
                        url: link,
                        data: "id=" + id,
                        success: function (response) {
                            $('body').loading('toggle');
                        }
                    }).success(function (response) {
                        if (response === 'YES') {
                            swal("DONE");
                            location.reload();
                        }else {
                            swal("Lỗi!");
                        }
                    });
                });
                return false;
            }
        });
    },

    initPerPage: function () {
        // limit and order
        $(document).on('change', '#limit_pagination', function () {
            $('#form_custom_list').submit();
        });
    },

    initFilter: function () {
        // $(document).on('change', '.filter_header', function () {
        //     $('#form_custom_list').submit();
        // });
        var el = document.getElementById("mobile");

        //---------------------------------Chỉ cho nhập số---------------------------------
        if (el) {
            el.addEventListener("keypress", function (evt) {
                if (
                    (evt.which != 8 && evt.which != 0 && evt.which < 48) ||
                    evt.which > 57
                ) {
                    evt.preventDefault();
                }
            });
        }

    },

    initActive: function () {

        $(".nav li").on("click", function () {
            $(".nav li").removeClass("active");
            $(this).addClass("active");
        });
    },

    initFilterDate: function () {

        $(".expand").on("click", function () {
            $(this).next().slideToggle(200);
            $expand = $(this).find(">:first-child");

            if ($expand.text() == "+") {
                $expand.text("-");
            } else {
                $expand.text("+");
            }
        });

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            var startDate = $('#startDate').val();
            var endDate = $('#endDate').val();
            if (startDate && endDate) {
                $('#reservation').val(startDate + ' - ' + endDate);
            } else {
                $('#reservation').val('');
            }

        }

        $('#reservation').daterangepicker({
            startDate: start,
            endDate: end,
            minDate: moment().subtract(2, 'years'),
            maxDate: moment(),
            dateLimit: {days: 730},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Tháng này': [moment().startOf('month'), moment()],
                'Tháng trước': [moment().subtract(1, 'months').startOf('month'), moment().subtract(1, 'months').endOf('month')],
                '6 tháng trước': [moment().subtract(6, 'months'), moment()],
                'Năm nay': [moment().startOf('year'), moment()],
                'Năm trước': [moment().subtract(1, 'years').startOf('year'), moment().subtract(1, 'years').endOf('year')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'Từ',
                toLabel: 'Đến',
                format: 'DD/MM/YYYY',
                customRangeLabel: 'Tùy chỉnh',
                // daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                daysOfWeek: ['Cn', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                // monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                firstDay: 1
            }
        }, cb);
        cb(start, end);

        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language: 'vn',
            dates: {
                days: ["Chủ nhật", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Tháng 1", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                today: "Today",
                clear: "Clear",
                titleFormat: "MM yyyy"
            }
        });

        function cbClass(start, end) {
            var startDate = $('.startDate').val();
            var endDate = $('.endDate').val();
            if (startDate && endDate) {
                $('.reservation').val(startDate + ' - ' + endDate);
            } else {
                $('.reservation').val('');
            }

        }

        $('.reservation').daterangepicker({
            startDate: start,
            endDate: end,
            // minDate: moment().subtract(2, 'years'),
            // maxDate: moment(),
            dateLimit: {days: 730},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Tháng này': [moment().startOf('month'), moment()],
                'Tháng trước': [moment().subtract(1, 'months').startOf('month'), moment().subtract(1, 'months').endOf('month')],
                '6 tháng trước': [moment().subtract(6, 'months'), moment()],
                'Năm nay': [moment().startOf('year'), moment()],
                'Năm trước': [moment().subtract(1, 'years').startOf('year'), moment().subtract(1, 'years').endOf('year')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'Từ',
                toLabel: 'Đến',
                format: 'DD/MM/YYYY',
                customRangeLabel: 'Tùy chỉnh',
                // daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                daysOfWeek: ['Cn', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                // monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                firstDay: 1
            }
        }, cbClass);
        cbClass(start, end);
    },

    initTooltip: function () {
        $('[data-toggle="popover"]').popover();

        $('#name').change(function () {
            var title, slug;

            //Lấy text từ thẻ input title
            str = document.getElementById("name");
            if (str != null) {
                title = str.value;
            }
            else {
                title = null;
            }

            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();

            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            document.getElementById('slug').value = slug;
        });
    },

};

Main.fn.initdatetimepicker();
Main.fn.initCheckbox();
Main.fn.initItemActions();
Main.fn.ininchecknumber();
Main.fn.initPerPage();
Main.fn.initFilter();
Main.fn.initFilterDate();
Main.fn.initActive();
Main.fn.initimage();
Main.fn.initTooltip();


// function ChangeToSlug()
// {
//     var title, slug;
//     //Lấy text từ thẻ input title
//     str = document.getElementById("name");
//     if (str != null) {
//         title = str.value;
//     }
//     else {
//         title = null;
//     }
//
//     //Đổi chữ hoa thành chữ thường
//     slug = title.toLowerCase();
//
//     //Đổi ký tự có dấu thành không dấu
//     slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
//     slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
//     slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
//     slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
//     slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
//     slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
//     slug = slug.replace(/đ/gi, 'd');
//     //Xóa các ký tự đặt biệt
//     slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
//     //Đổi khoảng trắng thành ký tự gạch ngang
//     slug = slug.replace(/ /gi, "-");
//     //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
//     //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
//     slug = slug.replace(/\-\-\-\-\-/gi, '-');
//     slug = slug.replace(/\-\-\-\-/gi, '-');
//     slug = slug.replace(/\-\-\-/gi, '-');
//     slug = slug.replace(/\-\-/gi, '-');
//     //Xóa các ký tự gạch ngang ở đầu và cuối
//     slug = '@' + slug + '@';
//     slug = slug.replace(/\@\-|\-\@|\@/gi, '');
//     //In slug ra textbox có id “slug”
//     document.getElementById('slug').value = slug;
// }


