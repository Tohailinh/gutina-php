$(document).ready(function(){
    $('#owl-carousel-1').owlCarousel({
        responsive:{
            0:{
                items: 3
            },
            768:{
                items: 4
            },
            1024:{
                items: 6
            }
        },
        loop: true,
        margin: 30,
    });

    $('#owl-carousel-2').owlCarousel({
        margin: 30,
        loop: true,
        responsive:{
            0:{
                items: 1
            },
            768:{
                items: 2
            },
            1025:{
                items: 3
            }
        },
    });

    var status = false;

    $("#btn_menu").click(function(){
        status = !status;
        if(status === true){
            $("body").css("overflow", "hidden");
        }else{
            $("body").css("overflow", "scroll");
        }
    });

    $(".btn-cancel").click(function(){
        popup();
    });

    $("#btn-modal").click(function(){
        popup();
    });

    $(".popup").click(function(){
        popup();
    });

    var owl_1 = $('#owl-carousel-1');
    var owl_2 = $('#owl-carousel-2');

    owl_1.owlCarousel();
    owl_2.owlCarousel();

    $(".prev_carousel_1").click(function(){
        owl_1.trigger('prev.owl.carousel');
    });

    $(".next_carousel_1").click(function(){
        owl_1.trigger('next.owl.carousel');
    });

    $(".prev_carousel_2").click(function(){
        owl_2.trigger('prev.owl.carousel');
    });

    $(".next_carousel_2").click(function(){
        owl_2.trigger('next.owl.carousel');
    });

    function popup() {
        status = !status;
        $( "#bs-example-navbar-collapse-1" ).removeClass( "in" );
        $("body").css("overflow", "scroll");
    }

    var page = 1;

    $(".load-more").click(function() {
        page++;
        $.ajax({
            url: "/load-more/"+ page,
            success: function(result){
                if(result.is_last == 1){
                    $(".load-more").hide();
                }
            $("._content_news").append(result.data);
        }});
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(".ct-gutina").click(function() {
        window.location.href = "http://chuyentien.gutina.vn";
    })

    // window.addEventListener("load", function(event) {
    //     lazyload();
    // });

    $(document).ready(function() {
        $('img').lazyload({
            threshold : 200,
            effect: 'fadeIn'
        });
    });

    //Đối tác đăng kí
    $(document).on('click', '.btn-contract', function () {
        var rank_salary = $('#rank_salary').val();
        var address = $('#address').val();
        var name = $('#name').val();
        var phone = $('#phone').val();
        var email = $('#email').val();
        if(rank_salary == ''||address == ''||name == ''||phone == ''||email == '')
        {
            alert("Vui lòng nhập đầy đủ thông tin!!");
        }else {
            $.ajax({
                type: "POST",
                url: "/doi-tac",
                data : {
                    'rank_salary': rank_salary,
                    'address': address,
                    'name': name,
                    'phone': phone,
                    'email': email
                },
                cache: false,
                success: function(result){
                    if (result === 'YES') {
                        $('#myModal').modal('hide');
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Đăng kí thành công!',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        // location.reload();
                    }else {
                        $('#myModal').modal('hide');
                        swal("Lỗi!");
                    }
                }
            });
        }


    });

});