<?php

namespace App\Controllers;

use App\Models\Category;
use App\Models\Contract;
use App\Models\News;
use voku\helper\AntiXSS;

class HomeController extends BaseController
{
    function __construct()
    {
        $this->folder = 'fontend';
    }

    public function index()
    {
        $this->render('home', 'layout_home', '');
    }

    public function lienHe()
    {
        $this->render('lien-he', 'layout_home', '');
    }

    public function hoiDap()
    {
        $this->render('hoi-dap', 'layout_home', '');
    }

    public function doiTac()
    {
        $rank_salary = $this->make_safe($_POST["rank_salary"]);
        $address = $this->make_safe($_POST["address"]);
        $name = $this->make_safe($_POST["name"]);
        $phone = $this->make_safe($_POST["phone"]);
        $email = $this->make_safe($_POST["email"]);
        $data = ['rank_salary' => "'$rank_salary'", 'address' => "'$address'", 'name' => "'$name'", 'phone' => (int)$phone, 'email' => "'$email'"];
        if (Contract::save('contract', $data) == 1) {
            echo "YES";die;
        } else {
            echo "NO";die;
        }
    }

    public function rutTien()
    {
        $this->render('rut-tien', 'layout_home', '');
    }

    public function nopTien()
    {
        $this->render('nop-tien', 'layout_home', '');
    }

    public function tinTuc()
    {
        $where = 1;
        $where .= " AND parent_id = 0";
        $categories = Category::readAll('category', $where);
        $arr_slug = [];
        foreach ($categories as $k => $category) {
            $arr_slug[] = $category->slug;
        }

        if (!empty($_SERVER['REQUEST_URI'])){
            $url_sub = substr($_SERVER['REQUEST_URI'],1);
        }

        $url = explode("?", $url_sub);
        if (!empty($url)){
            $slug = $url[0];
        }

        if (!in_array($slug, $arr_slug)) {
            header('Location: /admin/Pages/error');
        }

        $page = isset($_GET["page"])?(int)$_GET["page"]: 1;
        $countItems = News::loadCount($slug);

        $antiXss = new AntiXSS();
        $this->render('tin-tuc', 'layout_home', ['antiXss' => $antiXss, 'categories' => $categories, 'slug' => $slug, 'page' => $page, 'countItems' => $countItems]);
    }

    public function chiTietTinTuc()
    {
        $slug = substr($_SERVER['REQUEST_URI'], 9);
        $news_detail = News::findDetail($slug, 'news');
        $news_random = News::newsRandom($news_detail->category_id);
        $antiXss = new AntiXSS();
        $this->render('chi-tiet-tin-tuc', 'layout_home', ['slug' => $slug, 'news_detail' => $news_detail, 'news_random' => $news_random, 'antiXss' => $antiXss]);
    }
}

?>