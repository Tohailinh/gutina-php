<?php
/**
 * Created by PhpStorm.
 * User: AoE_Test
 * Date: 12/10/2018
 * Time: 2:41 PM
 */

namespace App\Controllers;
class BaseController
{
    protected $folder; // Biến có giá trị là thư mục nào đó trong thư mục views, chứa các file view template của phần đang truy cập.
    // Hàm hiển thị kết quả ra cho người dùng.
    function render($file, $layout, $data = array())
    {
        // Kiểm tra file gọi đến có tồn tại hay không?
        $view_file = '../Views/' . $this->folder . '/' . $file . '.php';
        if (is_file($view_file)) {
            // Nếu tồn tại file đó thì tạo ra các biến chứa giá trị truyền vào lúc gọi hàm
            extract($data);

            // Sau đó lưu giá trị trả về khi chạy file view template với các dữ liệu đó vào 1 biến chứ chưa hiển thị luôn ra trình duyệt
            ob_start();
            require_once($view_file);
            $content = ob_get_clean();
            // Sau khi có kết quả đã được lưu vào biến $content, gọi ra template chung của hệ thống đế hiển thị ra cho người dùng
            if (empty($layout)){
                $layout = 'master';
            }

            $layout_ro = '../Views/layouts/'.$layout.'.php';
            require_once($layout_ro);
        } else {
            // Nếu file muốn gọi ra không tồn tại thì chuyển hướng đến trang báo lỗi.
            header('Location: /admin/Pages/error');exit();
        }
    }

    function redirect($url , $datas = array() , $statusCode = 303)
    {
        if (!empty($datas)){
            if (!empty($datas['request'])){
                $_SESSION['request'] = $datas['request'];
            }
            if (!empty($datas['errors'])){
                $_SESSION['errors'] = $datas['errors'];
            }

            if (!empty($datas['messages'])){
                $_SESSION['messages'] = $datas['messages'];
            }
        }


        header('Location: ' . $url, true, $statusCode);
        die();
    }

    function vn_to_str ($str){
        $unicode = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd'=>'đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D'=>'Đ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach($unicode as $nonUnicode=>$uni){
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }
        $str = str_replace(' ','_',$str);
        return $str;
    }

    function escape ($string){
        return htmlspecialchars($string , ENT_QUOTES , 'UTF-8');
    }

    function make_safe($variable)
    {
        global $connect1;
        $variable = strip_tags(mysqli_real_escape_string($connect1 , trim($variable)));
        return $variable;
    }


}