<?php

namespace App\Controllers;

use App\Models\Contract;
use App\Models\Menu;
use voku\helper\AntiXSS;
session_start();

class MenuController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'menu';
    }

    public function index()
    {
        $name = !empty($_GET['name']) ? trim($_GET['name']) : [];

        if (!empty($_GET['name'])){
            //'$options' => "i" không phân biệt chữ hoa chữ thường
            $where = ["name" =>  ['$regex' => $name, '$options' => "i"]];
        }else{
            $where = [];
        }

        $menus = Menu::getAll( 'menu',$where);
        $antiXss = new AntiXSS();
        $this->render('index', '', ['menus' => $menus, 'antiXss' => $antiXss]);
    }

    public function show()
    {
        $menu = Menu::getById($this->make_safe($_GET['id']),'menu');
        if (empty($menu)) {
            $errors[] = 'Menu không tồn tại!';
            $this->redirect('/admin/Menu/index' , ['errors' => $errors]);
        }

        $antiXss = new AntiXSS();
        $this->render('show', '', [ 'menu' => $menu, 'antiXss' => $antiXss]);
    }

    public function create()
    {
        if ($_POST["submit"]) {
            $data = [];

            $data['name'] = $_POST['name'];
            $data['parent'] = 1;

            if (empty($data['name'])) {
                $errors[] = "Tên menu không được để trống!";
            }

            if (empty($errors)) {
                if (Menu::createOneMongo('menu', $data)) {
                    $messages[] = 'Thêm mới menu thành công!';
                    $this->redirect('/admin/Menu/index', ['messages' => $messages]);
                } else {
                    $this->render('create', '', ['errors' => $errors]);
                }
            }
        }

        $antiXss = new AntiXSS();
        $this->render('create', '', ['errors' => $errors,'antiXss' => $antiXss]);
    }

    public function edit()
    {
        $id = $this->make_safe($_GET['id']);
        $menu = Menu::getById($id,'menu');
        if ($_POST["submit"]) {
            $data = [];
            $data['name'] = $this->make_safe($_POST['name']);
            $errors = [];
            $messages = [];
            if (empty($data['name'])) {
                $errors[] = "Tên menu không được để trống!";
            }

            $save = Menu::updateOneMongo($id,'menu',$data);

            if (!empty($data['name']) && empty($errors)) {
                if ($save) {
                    $messages[] =  'Cập nhập thành công!';
                    $this->redirect('/admin/Menu/index' , ['messages' => $messages]);
                } else {
                    $this->render('edit', '', ['errors' => $errors]);
                }
            }
        }
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['errors' => $errors, 'menu' => $menu, 'antiXss' => $antiXss]);
    }


    //action items
    public function itemActions()
    {
        $id = $_GET['id'];
        $data = Menu::getById($this->make_safe($id),'menu');

        if ($data == false) {
            echo "NO";die;
        }

        Menu::deleteMongo($id,'menu');
        echo "YES";die;
    }

}

?>