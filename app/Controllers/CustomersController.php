<?php

namespace App\Controllers;

use App\Models\Customers;
use voku\helper\AntiXSS;

session_start();

class CustomersController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'customers';
    }

    public function index()
    {
        $customer_name = !empty($_GET['customer_name']) ? trim($_GET['customer_name']) : '';
        $filterStatus = isset($_GET['filter_status']) ? $_GET['filter_status'] : 'none';
        $where = 1;
        if ($filterStatus != "none") {
            $where .= " AND status = " . (int)$filterStatus . " ";
        }

        if ($customer_name != "") {
            $where .= " AND customer_name LIKE '%" . $customer_name . "%' ";
        }

        $customers = Customers::readAll('customers',$where);
        $antiXss = new AntiXSS();
        $this->render('index', '', ['customers' => $customers, 'customer_name' => $customer_name, 'filterStatus' => $filterStatus, 'antiXss' => $antiXss]);
    }
    public function create()
    {
        $antiXss = new AntiXSS();
        $this->render('create', '', ['antiXss' => $antiXss]);
    }


    public function store()
    {
        if ($_POST["submit"]) {
            $customer_name = $this->make_safe($_POST["customer_name"]);
            $job = $this->make_safe($_POST["job"]);
            $title = $this->make_safe($_POST["title"]);
            $content = $this->make_safe($_POST["content"]);
            $rate = (int)$_POST["rate"];
            $status = (int)$_POST["status"];
            $errors = [];
            $messages = [];

            //check tên khách hàng
            if (empty($customer_name)) {
                $errors[] = "Vui lòng nhập tên khách hàng!";
            }

            $filename = date("Y-m-d-H-i-s");
            $urlImage = $avatar = "";
            $target_dir = "uploads/customers/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $filetype = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check != false) {
                if ($filetype == "jpg" or $filetype == "jpeg") $avatar = $filename . ".jpg";
                elseif ($filetype == "png") $avatar = $filename . ".png";
                elseif ($filetype == "gif") $avatar = $filename . ".gif";
                elseif ($filetype == "") $avatar = "";

                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, TRUE);
                }

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir . $avatar)) {
                    $urlImage = $avatar;
                } else {
                    $errors[] = "Xin lỗi, đã xảy ra lỗi khi tải lên tệp của bạn!";
                }
            } else {
                $urlImage = '';
            }

            $data = ['customer_name' => "'$customer_name'", 'avatar' => "'$urlImage'" , 'job' => "'$job'", 'title' => "'$title'", 'rate' => (int)$rate, 'content' => "'$content'", 'status' => (int)$status];
            if (empty($errors)) {
                if (Customers::save('customers',$data) == 1) {
                    $messages[] = 'Thêm mới khách hàng thành công!';
                    $this->redirect('/admin/Customers/index', ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/Customers/create', ['errors' => $errors, 'request' => $_POST]);
                }
            } else {
                $this->redirect('/admin/Customers/create', ['errors' => $errors, 'request' => $_POST]);
            }
        }
        $antiXss = new AntiXSS();
        $this->render('create', '', ['errors' => $errors, 'antiXss' => $antiXss]);
    }

    public function edit()
    {
        $customers = Customers::find($this->make_safe($_GET['id']),'customers');
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['antiXss' => $antiXss, 'customers' => $customers]);
    }

    public function update()
    {
        session_start();
        $customers = Customers::find($this->make_safe($_GET['id']),'customers');
        if ($_POST["submit"]) {
            $customer_name = $this->make_safe($_POST["customer_name"]);
            $job = $this->make_safe($_POST["job"]);
            $title = $this->make_safe($_POST["title"]);
            $content = $this->make_safe($_POST["content"]);
            $rate = (int)$_POST["rate"];
            $status = (int)$_POST["status"];
            $errors = [];

            //check tên khách hàng
            if (empty($customer_name)) {
                $errors = "Vui lòng nhập tên khách hàng!";
            }

            //check số sao
            if ($rate == 0) {
                $errors[] = "Vui lòng chọn số sao!";
            }

            //check trạng thái
            if ($status != 1 && $status != 0) {
                $errors[] = "Vui lòng chọn trạng thái!";
            }

            $filename = date("Y-m-d-H-i-s");
            $urlImage = $avatar = "";
            $uploadPath = $_SERVER['HTTP_ORIGIN'] . '/uploads/customers/';
            $target_dir = "uploads/customers/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $filetype = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check != false) {
                if ($filetype == "jpg" or $filetype == "jpeg") $avatar = $filename . ".jpg";
                elseif ($filetype == "png") $avatar = $filename . ".png";
                elseif ($filetype == "gif") $avatar = $filename . ".gif";
                elseif ($filetype == "") $avatar = "";
                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, TRUE);
                }

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir . $avatar)) {
                    $urlImage = $avatar;
                } else {
                    $errors[] = "Xin lỗi, đã xảy ra lỗi khi tải lên tệp của bạn!";
                }
            } else {
                $urlImage = $customers->avatar;
            }

            $data = ['id' => $_GET['id'],'customer_name' => $customer_name, 'avatar' => $urlImage , 'job' => $job, 'title' => $title, 'rate' => (int)$rate, 'content' => $content, 'status' => (int)$status];

            if (!empty($customer_name) && empty($errors)) {
                if (Customers::save('customers',$data) < 2) {
                    $messages[] = 'Cập nhập thành công!';
                    $this->redirect('/admin/Customers/index', ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/Customers/edit?id='.$_GET['id'] , ['errors' => $errors, 'request' => $_POST]);
                }
            }else{
                $this->redirect('/admin/Customers/edit?id='.$_GET['id'] , ['errors' => $errors, 'request' => $_POST]);
            }
        }
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['errors' => $errors, 'customers' => $customers, 'antiXss' => $antiXss]);
    }

    public function show()
    {
        $customers = Customers::find($this->make_safe($_GET['id']),'customers');
        $errors = [];
        if (empty($customers->id)) {
            $errors[] = 'Quản trị viên không tồn tại!';
            $errors[] = 'Danh mục không tồn tại!';
            $this->redirect('/admin/Customers/index', ['errors' => $errors]);
        }
        $antiXss = new AntiXSS();
        $this->render('show', '', ['errors' => $errors, 'customers' => $customers, 'antiXss' => $antiXss]);
    }

    //action items
    public function itemActions()
    {
        $id = $_GET['id'];
        $data = Customers::find($this->make_safe($id),'customers');

        if ($data == false) {
            echo 'NO';die;
        }

        $data->delete('customers',$id);
        echo 'YES';die;
    }


}

?>