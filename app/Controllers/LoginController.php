<?php

namespace App\Controllers;

use App\Models\Login;
use voku\helper\AntiXSS;

class LoginController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'auth';
    }

    public function login()
    {
        $antiXss = new AntiXSS();
        if ($_POST["login"]) {
            $uname = $antiXss->xss_clean($this->make_safe((string)$_POST["username"]));
            $pname = $antiXss->xss_clean($this->make_safe((string)$_POST["password"]));
            $errors = [];
            if (empty($uname)) {
                $errors[] = "Tài khoản là trường bắt buộc!";
            }
            if (empty($pname)) {
                $errors[] = "Mật khẩu là trường bắt buộc!";
            }

            $username = htmlentities($uname);
            $password = md5(htmlentities($pname));
            if (count($errors) == 0) {
                $sqlQuery = Login::_login($username, $password);
                if ($sqlQuery != null) {
                    if (mysqli_num_rows($sqlQuery) == 1) {
                        $rows = mysqli_fetch_array($sqlQuery);
                        $_SESSION["username"] = $rows["username"];
                        $_SESSION["avatar"] = $rows["avatar"];
                        $_SESSION["fullname"] = $rows["fullname"];
                        $_SESSION["adminid"] = $rows["id"];
                        $_SESSION['loggedin_time'] = time();
                        $_SESSION["login"] = true;
                        header('Location: /admin/Admin/index');
                    } else {
                        $errors[] = 'Sai tài khoản hoặc mật khẩu';
                        $this->render('login', 'layout_login', ['errors' => $errors]);
                    }
                } else {
                    $errors[] = 'Sai tài khoản hoặc mật khẩu';
                    $this->render('login', 'layout_login', ['errors' => $errors]);
                }
            } else {
                $this->render('login', 'layout_login', ['errors' => $errors]);
            }
        }

        $this->render('login', 'layout_login', ['errors' => $errors]);

    }

    public function logout()
    {
        unset($_SESSION["username"]);
        unset($_SESSION["fullname"]);
        unset($_SESSION["userid"]);
        unset($_SESSION["adminid"]);
        unset($_SESSION["login"]);
        session_destroy();
        header('Location: /admin/Login/login');
    }
}

?>