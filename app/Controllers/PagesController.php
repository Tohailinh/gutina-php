<?php
namespace App\Controllers;

class PagesController extends BaseController {
    function __construct()
    {
        $this->folder = 'layouts';
    }

    public function error()
    {
        $this->render('error', 'layout_error','');
    }

}
?>