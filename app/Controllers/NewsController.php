<?php

namespace App\Controllers;

use App\Models\News;
use voku\helper\AntiXSS;
session_start();

class NewsController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'news';
    }

    public function index()
    {
        $name = !empty($_GET['name']) ? trim($_GET['name']) : '';
        $filterStatus = isset($_GET['filter_status']) ? $_GET['filter_status'] : 'none';

        $where = 1;
        if ($filterStatus != "none") {
            $where .= " AND status = " . (int)$filterStatus . " ";
        }

        if ($name != "") {
            $where .= " AND name LIKE '%" . $name . "%' ";
        }

        $news = News::readAll('news', $where);
        $antiXss = new AntiXSS();
        $this->render('index', '', ['news' => $news, 'name' => $name, 'filterStatus' => $filterStatus, 'antiXss' => $antiXss]);
    }

    public function create()
    {
        $antiXss = new AntiXSS();
        $this->render('create', '', ['antiXss' => $antiXss]);
    }

    public function store()
    {
        if ($_POST["submit"]) {
            $name = $this->make_safe($_POST["name"]);
            $slug = $this->make_safe($_POST["slug"]);
            $description = $this->make_safe($_POST["description"]);
            $key_seo = $this->make_safe($_POST["key_seo"]);
            $content = $_POST["content"];
            $category_id = (int)$_POST["category_id"];
            $status = (int)$_POST["status"];
            $errors = [];

            //check tên khách hàng
            if (empty($name)) {
                $errors[] = "Vui lòng nhập tiêu đề!";
            }

            //check tên khách hàng
            if (empty($key_seo)) {
                $errors[] = "Vui lòng nhập key seo!";
            }

            //check số sao
            if ($category_id == 0) {
                $errors[] = "Vui lòng chọn danh mục!";
            }

            //check trạng thái
            if ($status != 1 && $status != 0) {
                $errors[] = "Vui lòng chọn trạng thái!";
            }

            $filename = date("Y-m-d-H-i-s");
            $urlImage = $avatar = "";
//            $uploadPath = $_SERVER['HTTP_ORIGIN'] . '/uploads/news/';

            $target_dir = "uploads/news/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $filetype = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check != false) {
                if ($filetype == "jpg" or $filetype == "jpeg") $avatar = $filename . ".jpg";
                elseif ($filetype == "png") $avatar = $filename . ".png";
                elseif ($filetype == "gif") $avatar = $filename . ".gif";
                elseif ($filetype == "") $avatar = "";

                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, TRUE);
                }

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir . $avatar)) {
                    $urlImage = $avatar;
                } else {
                    $errors[] = "Xin lỗi, đã xảy ra lỗi khi tải lên tệp của bạn!";
                }
            } else {
                $urlImage = '';
            }
            $data = ['avatar' => "'$urlImage'", 'name' => "'$name'", 'slug' => "'$slug'", 'description' => "'$description'",
                'key_seo' => "'$key_seo'", 'content' => "'$content'", 'status' => "'$status'", 'category_id' => "'$category_id'"];

            if (empty($errors)) {
                if (News::save('news',$data) == 1) {
                    $messages[] = 'Thêm mới tin tức thành công!';
                    $this->redirect('/admin/News/index', ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/News/create', ['errors' => $errors, 'request' => $_POST]);
                }
            }else {
                $this->redirect('/admin/News/create', ['errors' => $errors, 'request' => $_POST]);
            }
        }
        $antiXss = new AntiXSS();
        $this->render('create', '', ['errors' => $errors, 'antiXss' => $antiXss]);
    }

    public function edit()
    {
        $news = News::find($this->make_safe($_GET['id']), 'news');
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['antiXss' => $antiXss, 'news' => $news]);
    }

    public function update()
    {
        $news = News::find($this->make_safe($_GET['id']), 'news');
        if ($_POST["submit"]) {
            $name = $this->make_safe($_POST["name"]);
            $slug = $this->make_safe($_POST["slug"]);
            $description = $this->make_safe($_POST["description"]);
            $key_seo = $this->make_safe($_POST["key_seo"]);
            $content = $_POST["content"];
            $category_id = (int)$_POST["category_id"];
            $status = (int)$_POST["status"];
            $errors = [];

            //check tên khách hàng
            if (empty($name)) {
                $errors = "Vui lòng nhập tiêu đề!";
            }

            //check tên khách hàng
            if (empty($key_seo)) {
                $errors = "Vui lòng nhập key seo!";
            }

            //check số sao
            if ($category_id == 0) {
                $errors[] = "Vui lòng chọn danh mục!";
            }

            //check trạng thái
            if ($status != 1 && $status != 0) {
                $errors[] = "Vui lòng chọn trạng thái!";
            }

            $filename = date("Y-m-d-H-i-s");
            $urlImage = $avatar = "";
            $target_dir = "uploads/news/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $filetype = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check != false) {
                if ($filetype == "jpg" or $filetype == "jpeg") $avatar = $filename . ".jpg";
                elseif ($filetype == "png") $avatar = $filename . ".png";
                elseif ($filetype == "gif") $avatar = $filename . ".gif";
                elseif ($filetype == "") $avatar = "";
                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, TRUE);
                }

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir . $avatar)) {
                    $urlImage = $avatar;
                } else {
                    $errors[] = "Xin lỗi, đã xảy ra lỗi khi tải lên tệp của bạn!";
                }
            } else {
                $urlImage = $news->avatar;
            }
            $data = ['id' => $_GET['id'], 'avatar' => $urlImage, 'name' => $name, 'slug' => $slug, 'description' => $description,
                'key_seo' => $key_seo, 'content' => $content, 'status' => $status, 'category_id' => $category_id];
            if (!empty($name) && empty($errors)) {
                if (News::save('news',$data) < 2) {
                    $messages[] = 'Cập nhập thành công!';
                    $this->redirect('/admin/News/index', ['messages' => $messages]);
                } else {
                    $this->render('edit', '', ['errors' => $errors]);
                }
            }
        }
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['errors' => $errors, 'news' => $news, 'antiXss' => $antiXss]);
    }

    public function show()
    {
        $news = News::find($this->make_safe($_GET['id']), 'news');
        $errors = [];
        if (empty($news->id)) {
            $errors[] = 'Quản trị viên không tồn tại!';
            $errors[] = 'Danh mục không tồn tại!';
            $this->redirect('/admin/News/index', ['errors' => $errors]);
        }
        $antiXss = new AntiXSS();
        $this->render('show', '', ['errors' => $errors, 'news' => $news, 'antiXss' => $antiXss]);
    }

    //action items
    public function itemActions()
    {
        $id = $this->make_safe($_GET['id']);
        $data = News::find($id, 'news');
        if ($data == false) {
            echo 'NO';die;
        }
        $data->delete('news',$id);
        echo 'YES';die;
    }

}

?>