<?php

namespace App\Controllers;

use App\Models\Category;
use function var_dump;
use voku\helper\AntiXSS;
session_start();

class CategoryController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'category';
    }

    public function index()
    {
        $name = !empty($_GET['name']) ? trim($_GET['name']) : '';

        $where = 1;
        if ($name != "") {
            $where .= " AND name LIKE '%" . $name . "%' ";
        }
        $categories = Category::readAll('category', $where);
        $antiXss = new AntiXSS();

        $this->render('index', '', ['categories' => $categories, 'name' => $name, 'antiXss' => $antiXss]);
    }

    public function create()
    {
        $_SESSION['check_update'] = false;
        $antiXss = new AntiXSS();
        $this->render('create', '', ['antiXss' => $antiXss]);
    }

    public function store()
    {
        if ($_POST["submit"]) {
            $name = $this->make_safe($_POST["name"]);
            $slug = $this->make_safe($_POST["slug"]);
            $meta_title =$this->make_safe($_POST["meta_title"]);
            $meta_description = $this->make_safe($_POST["meta_description"]);
            $parent_id = $this->make_safe($_POST["parent_id"]);
            $errors = [];
            $messages = [];

            if (empty($name)) {
                $errors[] = "Tên danh mục không được để trống!";
            }

            if (empty($meta_title)) {
                $errors[] = "Tiêu đề không được để trống!";
            }

            if (empty($slug)) {
                $errors[] = "Slug không được để trống!";
            }

            $data = ['name' => "'$name'", 'slug' => "'$slug'", 'meta_title' => "'$meta_title'", 'meta_description' => "'$meta_description'", 'parent_id' => "'$parent_id'"];

            if (empty($errors)) {
                if (Category::save('category',$data) == 1) {
                    $messages[] =  'Thêm mới danh mục thành công!';
                    $this->redirect('/admin/Category/index' , ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/Category/create', ['errors' => $errors, 'request' => $_POST]);
                }
            }else{
                $this->redirect('/admin/Category/create', ['errors' => $errors, 'request' => $_POST]);
            }
        }

        $antiXss = new AntiXSS();
        $this->render('create', '', ['errors' => $errors, 'antiXss' => $antiXss]);
    }

    public function edit()
    {
        $category = Category::find($_GET['id'],'category');
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['antiXss' => $antiXss, 'category' => $category]);
    }

    public function update()
    {
        session_start();
        $category = Category::find($this->make_safe($_GET['id']),'category');
        if ($_POST["submit"]) {
            $name = $this->make_safe($_POST["name"]);
            $slug = $this->make_safe($_POST["slug"]);
            $meta_title = $this->make_safe($_POST["meta_title"]);
            $meta_description = $this->make_safe($_POST["meta_description"]);
            $parent_id = $this->make_safe($_POST["parent_id"]);
            $errors = [];
            $messages = [];

            if (empty($name)) {
                $errors[] = "Tên danh mục không được để trống!";
            }

            if (empty($meta_title)) {
                $errors[] = "Tiêu đề không được để trống!";
            }

            if (empty($slug)) {
                $errors[] = "Slug không được để trống!";
            }

            if ($parent_id == $category->id){
                $parent_id = 0;
            }

            $data = ['id' => $this->make_safe($_GET['id']), 'name' => $name, 'slug' => $slug, 'meta_title' =>$meta_title, 'meta_description' => $meta_description, 'parent_id' => $parent_id];

            if (!empty($name) && empty($errors)) {
                if (Category::save('category',$data) < 2) {
                    $messages[] =  'Cập nhập thành công!';
                    $this->redirect('/admin/Category/index' , ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/Category/edit?id='.$_GET['id'] , ['errors' => $errors, 'request' => $_POST]);
                }
            }else{
                $this->redirect('/admin/Category/edit?id='.$_GET['id'] , ['errors' => $errors, 'request' => $_POST]);
            }
        }
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['errors' => $errors, 'category' => $category, 'antiXss' => $antiXss]);
    }

    public function show()
    {
        $category = Category::find($this->make_safe($_GET['id']),'category');
        $errors = [];
        if (empty($category->id)) {
            $errors[] = 'Danh mục không tồn tại!';
            $this->redirect('/admin/Category/index' , ['errors' => $errors]);
        }
        $antiXss = new AntiXSS();
        $this->render('show', '', ['errors' => $errors, 'category' => $category, 'antiXss' => $antiXss]);
    }

    //action items
    public function itemActions()
    {
        $id = $_GET['id'];
        $data = Category::find($id, 'category');

        if ($data == false) {
            echo 'NO';
            die;
        }
        $data->delete('category',$id);
        echo 'YES';
        die;
    }


}

?>