<?php

namespace App\Controllers;

use App\Models\Admin;
use voku\helper\AntiXSS;

class AdminController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'admin';
    }

    public function index()
    {
        $email = !empty($_GET['email']) ? trim($_GET['email']) : '';
        $filterStatus = isset($_GET['filter_status']) ? $_GET['filter_status'] : 'none';

        $where = 1;
        if ($filterStatus != "none") {
            $where .= " AND status = " . (int)$filterStatus . " ";
        }

        if ($email != "") {
            $where .= " AND email LIKE '%" . $email . "%' ";
        }

        $admins = Admin::readAll('admin',$where);
        $antiXss = new AntiXSS();
        $this->render('index', '', ['admins' => $admins, 'antiXss' => $antiXss, 'email' => $email, 'filterStatus' => $filterStatus]);
    }

    public function create()
    {
        $_SESSION['check_update'] = false;
        $antiXss = new AntiXSS();
        $this->render('create', '', ['antiXss' => $antiXss]);
    }
    
    public function store()
    {
      $_SESSION['check_update'] = false;
        if ($_POST["submit"]) {
            $username = !empty($_POST["username"]) ? $this->vn_to_str($this->make_safe($_POST["username"])) : '';
            $password = !empty($_POST["password"]) ?  md5($this->make_safe($_POST["password"])) : '';
            $email = !empty($_POST["email"]) ? $this->make_safe($_POST["email"]) : '';
            $fullname = $this->make_safe($_POST["fullname"]);
            $status = (int)$_POST["status"];

            $errors = [];
            $messages = [];

            //check tên đăng nhập
            if (empty($username)) {
                $errors[] = "Vui lòng nhập tài khoản!";
            } else {
                if (strlen($username) > 255){
                    $errors[] = "Tài khoản nhỏ hơn 255 kí tự!";
                }

                $countadmin = Admin::checkUsername($username);
                if ($countadmin > 0) {
                    $errors[] = "Tên đăng nhập đã tồn tại!";
                }
            }

            //check mật khẩu
            if (!empty($password)) {
                if (strlen($password) < 3) {
                    $errors[] = "Mật khẩu phải lớn hơn 3 ký tự!";
                }
                if (strlen($password) > 255) {
                    $errors[] = "Mật khẩu phải nhỏ hơn 255 ký tự!";
                }
            } else {
                $errors[] = 'Vui lòng nhập mật khẩu!';
            }

            //check trạng thái
            if ($status != 1 && $status != 0) {
                $errors[] = "Vui lòng chọn trạng thái!";
            }

            //check email
            if (empty($email)) {
                $errors[] = "Vui lòng nhập email!";
            } else {
                $countemail = Admin::checkUEmail($email);
                if ($countemail > 0) {
                    $errors[] = "Email đã tồn tại!";
                }
            }

            $filename = date("Y-m-d-H-i-s");
            $urlImage = $avatar = "";
//            $uploadPath = $_SERVER['HTTP_ORIGIN'] . '/uploads/users/';
            $target_dir = "uploads/users/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $filetype = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check != false) {
                if ($filetype == "jpg" or $filetype == "jpeg") $avatar = $filename . ".jpg";
                elseif ($filetype == "png") $avatar = $filename . ".png";
                elseif ($filetype == "gif") $avatar = $filename . ".gif";
                elseif ($filetype == "") $avatar = "";

                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, TRUE);
                }

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir . $avatar)) {
                    $urlImage = $avatar;
                } else {
                    $errors[] = "Xin lỗi, đã xảy ra lỗi khi tải lên tệp của bạn!";
                }
            } else {
                $urlImage = '';
            }

            $data = ['username' => "'$username'", 'password' => "'$password'", 'fullname' => "'$fullname'", 'status' => (int)$status, 'email' => "'$email'", 'avatar' => "'$urlImage'"];

            if (empty($errors)) {
                if (Admin::save('admin',$data) == 1) {
                    $messages[] = 'Thêm mới quản trị thành công!';
                    $this->redirect('/admin/Admin/index', ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/Admin/create', ['errors' => $errors, 'request' => $_POST]);
                }
            } else {
                $this->redirect('/admin/Admin/create', ['errors' => $errors, 'request' => $_POST]);
            }
        }

        $antiXss = new AntiXSS();
        $this->render('create', '', ['errors' => $errors, 'antiXss' => $antiXss]);
    }

    /**
     * @return string
     */
    public function edit()
    {
        $admin = Admin::find($_GET['id'],'admin');
        $_SESSION['check_update'] = true;
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['antiXss' => $antiXss, 'admin' => $admin]);
    }

    public function update()
    {
        $_SESSION['check_update'] = true;
        $admin = Admin::find($_GET['id'],'admin');
        if ($_POST["submit"]) {
//            ';DROP TABLE test;--
            $username =$this->vn_to_str($this->make_safe($_POST["username"]));
            $email = $this->make_safe($_POST["email"]);
            $fullname = $this->make_safe($_POST["fullname"]);
            $status = (int)$_POST["status"];
            $errors = [];

            if($admin->id == $_SESSION['adminid']){
                $errors[] = "Không thể chỉnh sửa chính mình!";
            }

            //change password
            if ($_POST["changepassword"] == "on") {
                $password = md5($this->make_safe(strip_tags($_POST["password"])));
                //check mật khẩu
                if ($password != "") {
                    if (strlen($password) < 3) {
                        $errors[] = "Mật khẩu phải lớn hơn 3 ký tự!";
                    }
                    if (strlen($password) > 255) {
                        $errors[] = "Mật khẩu phải nhỏ hơn 255 ký tự!";
                    }
                } else {
                    $errors[] = "Vui lòng nhập mật khẩu!";
                }
            } else {
                $password = $admin->password;
            }
            //check trạng thái
            if ($status != 1 && $status != 0) {
                $errors[] = "Vui lòng chọn trạng thái!";
            }

            $filename = date("Y-m-d-H-i-s");
            $urlImage = $avatar = "";
            $target_dir = "uploads/users/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $filetype = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check != false) {
                if ($filetype == "jpg" or $filetype == "jpeg") $avatar = $filename . ".jpg";
                elseif ($filetype == "png") $avatar = $filename . ".png";
                elseif ($filetype == "gif") $avatar = $filename . ".gif";
                elseif ($filetype == "") $avatar = "";
                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, TRUE);
                }

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_dir . $avatar)) {
                    $urlImage = $avatar;
                } else {
                    $errors[] = "Xin lỗi, đã xảy ra lỗi khi tải lên tệp của bạn!";
                }
            } else {
                $urlImage = $admin->avatar;
            }

            $data = ['id' => $_GET['id'],'username' => $username, 'password' => $password, 'fullname' => $fullname, 'status' => $status, 'email' => $email, 'avatar' => $urlImage];

            if (!empty($username) && empty($errors)) {
                if (Admin::save('admin',$data)  < 2) {
                    $messages[] = 'Cập nhập thành công!';
                    $this->redirect('/admin/Admin/index', ['messages' => $messages]);
                } else {
                    $this->redirect('/admin/Admin/edit?id='.$_GET['id'] , ['errors' => $errors, 'request' => $_POST]);
                }
            }else{
                $this->redirect('/admin/Admin/edit?id='.$_GET['id'] , ['errors' => $errors, 'request' => $_POST]);
            }
        }
        $antiXss = new AntiXSS();
        $this->render('edit', '', ['errors' => $errors, 'admin' => $admin, 'antiXss' => $antiXss]);
    }

    public function show()
    {
        $admin = Admin::find($_GET['id'],'admin');
        $errors = [];
        if (empty($admin->id)) {
            $errors[] = 'Quản trị viên không tồn tại!';
            $this->redirect('/admin/Admin/index', ['errors' => $errors]);
        }
        $antiXss = new AntiXSS();
        $this->render('show', '', ['errors' => $errors, 'admin' => $admin, 'antiXss' => $antiXss]);
    }

    //action items
    public function itemActions()
    {
        $id = $_GET['id'];
        $data = Admin::find($id,'admin');

        if($id == $_SESSION['adminid'] ){
            echo "NO";die;
        }

        if ($data == false) {
            echo "NO";die;
        }
        $data->delete('admin',$id);
        echo "YES";die;
    }


}

?>