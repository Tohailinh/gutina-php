<?php

namespace App\Controllers;

use App\Models\Contract;
use function var_dump;
use voku\helper\AntiXSS;
session_start();

class ContractController extends BaseController
{
    function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
        $this->folder = 'contract';
    }

    public function index()
    {
        $name = !empty($_GET['name']) ? trim($_GET['name']) : '';
        $where = 1;
        if ($name != "") {
            $where .= " AND name LIKE '%" . $name . "%' ";
        }
        $contracts = Contract::readAll('contract', $where);

        $antiXss = new AntiXSS();
        $this->render('index', '', ['contracts' => $contracts, 'name' => $name, 'antiXss' => $antiXss]);
    }

    public function show()
    {
        $contact = Contract::find($this->make_safe($_GET['id']),'contract');
        $errors = [];
        if (empty($contact->id)) {
            $errors[] = 'Đối tác không tồn tại!';
            $this->redirect('/admin/Contract/index' , ['errors' => $errors]);
        }
        $antiXss = new AntiXSS();
        $this->render('show', '', ['errors' => $errors, 'contract' => $contact, 'antiXss' => $antiXss]);
    }

    //action items
    public function itemActions()
    {
        $id = $_GET['id'];
        $data = Contract::find($this->make_safe($id),'contract');

        if ($data == false) {
            echo 'NO';die;
        }
        $data->delete('contract',$id);
        echo 'YES';die;
    }


}

?>