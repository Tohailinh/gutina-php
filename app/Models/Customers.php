<?php

namespace App\Models;

class Customers extends Model
{
    public $id;
    public $customer_name;
    public $avatar;
    public $job;
    public $title;
    public $rate;
    public $content;
    public $status;

    function __construct($news_data = [])
    {
        if (isset($news_data['id'])) {
            $this->id = $news_data['id'];
            $this->customer_name = $news_data['customer_name'];
            $this->avatar = $news_data['avatar'];
            $this->job = $news_data['job'];
            $this->title = $news_data['title'];
            $this->rate = $news_data['rate'];
            $this->content = $news_data['content'];
            $this->status = $news_data['status'];
        }
    }
}

?>