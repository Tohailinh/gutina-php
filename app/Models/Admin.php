<?php

namespace App\Models;

class Admin extends Model
{
    public $id;
    public $username;
    public $email;
    public $fullname;
    public $password;
    public $avatar;
    public $status;

    function __construct($news_data = [])
    {
        if (isset($news_data['id'])) {
            $this->id = $news_data['id'];
            $this->username = $news_data['username'];
            $this->email = $news_data['email'];
            $this->fullname = $news_data['fullname'];
            $this->password = $news_data['password'];
            $this->avatar = $news_data['avatar'];
            $this->status = $news_data['status'];
        }

    }
    public static function checkUsername($username = "")
    {
        global $connect1;
        $sqlQuery = null;
        $sql = " SELECT count(id) as 'count' FROM admin WHERE username='" . $username . "' ";
        $sqlQuery = mysqli_fetch_array(mysqli_query($connect1, $sql));
        return $sqlQuery["count"];
    }

    public static function checkUEmail($email = "")
    {
        global $connect1;
        $sqlQuery = null;
        $sql = " SELECT count(id) as 'count' FROM admin WHERE email='" . $email . "' ";
        $sqlQuery = mysqli_fetch_array(mysqli_query($connect1, $sql));
        return $sqlQuery["count"];
    }

}

?>