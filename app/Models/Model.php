<?php
namespace App\Models;

class Model
{
    protected $attributes = [];
    protected $model;
    protected $table = '';
    public $limit = 10;

    public function __construct($model , $attributes = [])
    {
        $this->model = $model;
        $this->attributes = $attributes;
    }

    // ********************************************************mysql********************************************************
    public function readAll1($table , $where = []){
        global $connect2;
        if ($where == ""){
            $where = 1;
        }
        $sql = " SELECT * FROM  " . $table . " WHERE $where ";
        $stmt = $connect2->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        foreach ($result as $item) {
            $list[] = new static($item);
        }
        return $list;
    }

    public function readAll($table , $where = []){
        global $connect1;
        if ($where == ""){
            $where = 1;
        }
        $sql = " SELECT * FROM  " . $table . " WHERE $where ";
        $stmt = $connect1->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        foreach ($result as $item) {
            $list[] = new static($item);
        }
        return $list;
    }

    public function find($id,$table)
    {
        global $connect1;
        $sqlNews = null;
        $sql = "SELECT * FROM " . $table . " WHERE id = ?";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $sqlresult = mysqli_fetch_array($result);
        if (empty($sqlresult)) {
            return false;
        }
        return new static($sqlresult);
    }

    public function insert($table, $data)
    {
        global $connect1;
        $columns = array_keys($data);
        $columnSql = implode(',', $columns);
        $bindingSql = implode(',', $data);
        $sql = "INSERT INTO $table ($columnSql) VALUES ($bindingSql)";
        $stmt = $connect1->prepare($sql);
        $stmt->execute();
        return mysqli_affected_rows($connect1);
    }

    public function update($table,$id, $data) {
        global $connect1;
        if (isset($data['id']))
            unset($data['id']);
        $columns = array_keys($data);
        $columns = array_map(function($columns,$data){
            return $columns.'='."'$data'";
        }, $columns, $data);
        $bindingSql = implode(',', $columns);
        $sql = "UPDATE $table SET $bindingSql WHERE id = ?";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param('s' , $id);
        $stmt->execute();
        return mysqli_affected_rows($connect1);
    }

    public function delete($table, $id)
    {
        global $connect1;
        $sql = " DELETE FROM $table WHERE id = ?";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param('s' , $id);
        $stmt->execute();
        return mysqli_affected_rows($connect1);
    }

    public function save($table, $data){
        if (isset($data['id'])){
            self::update($table, $data['id'], $data);
        }else{
            return self::insert($table, $data);
        }
    }

    public function pagination($link, $page, $sotrang){
        echo  "<nav aria-label='...'>";
        echo  "<ul class='pagination'>";

        $strDisabled_P = "";
        $strDisabled_N = "";
        $strDisabled_Fi = "";
        $strDisabled_Fa = "";

        $link_P = $link."?page=".($page-1);
        $link_N = $link."?page=".($page+1);
        $link_Fi = $link."?page=1";
        $link_Fa = $link."?page=".$sotrang;

        if($page == 1){ $strDisabled_P = "disabled"; $link_P = "#"; $strDisabled_Fi = "disabled"; $link_Fi = "#";}
        if($page == $sotrang){ $strDisabled_N = "disabled"; $link_N = "#"; $strDisabled_Fa = "disabled"; $link_Fa = "#";}

        echo "<li class='page-item ".$strDisabled_Fi."'><a class='page-link' href='".$link_Fi."'>Trang đầu</a></li>";
        echo "<li class='page-item ".$strDisabled_P."'><a class='page-link' href='".$link_P."' aria-label='Previous'><span aria-hidden='true'>&laquo;</span><span class='sr-only'>Previous</span></a></li>";
        if($page>3){
            echo "<li class='page-item disabled'><a class='page-link' href='#'>...</a></li>";
        }
        for($i=1; $i<=$sotrang; $i++){
            $strClass = "";
            $strCurrent = "";
            if($page == $i) {
                $strClass = " active";
                $strCurrent = " <span class='sr-only'>(current)</span>";
            }
            if($i>=$page-2 && $i<=$page+2){
                echo "<li class='page-item ".$strClass."'><a class='page-link' href='".$link."?page=".$i."'>".$i.$strCurrent."</a></li>";
            }
        }
        if($page<$sotrang-2){
            echo "<li class='page-item disabled'><a class='page-link' href='#'>...</a></li>";
        }
        echo "<li class='page-item ".$strDisabled_N."'><a class='page-link' href='".$link_N."' aria-label='Next'><span aria-hidden='true'>&raquo;</span><span class='sr-only'>Next</span></a></li>";
        echo "<li class='page-item ".$strDisabled_Fa."'><a class='page-link' href='".$link_Fa."'>Trang cuối</a></li>";
        echo "</ul>";
        echo "</nav>";
    }



    ///********************************************************Mongo db***********************************************************/
    public function getAll($collection, $where){
        global $connectdb;
        $table = $connectdb->selectCollection($collection);
        return $table->find($where);
    }

    //get by id
    public function getById($id,$collection){
        global $connectdb;
        if (strlen($id) == 24){
            $id = new \MongoDB\BSON\ObjectID($id);
        }
        $table = $connectdb->selectCollection($collection);
        $cursor  = $table->findOne(['_id' => $id ]);

        if (!$cursor)
            return false ;

        return $cursor;
    }

    //delete
    public function deleteMongo($id , $collection){
        global $connectdb;
        if (strlen($id) == 24)
            $id = new \MongoDB\BSON\ObjectID($id);

        $table = $connectdb->selectCollection($collection);
        $result = $table->deleteOne(['_id' => $id]);

        if (!$id)
            return false;

        return $result;
    }

    //update
    public function updateOneMongo($id , $collection , $data)
    {
        global $connectdb;

        if (strlen($id) == 24)
            $id = new \MongoDB\BSON\ObjectID($id);

        $table = $connectdb->selectCollection($collection);
        $result  = $table->updateOne(
            ['_id' => $id ],
            ['$set' => $data]
        );

        if (!$id)
            return false;

        return $result;
    }

    //create
    public function createOneMongo($collection , $data)
    {
        global $connectdb;

        $table = $connectdb->selectCollection($collection);
        return $result = $table->insertOne($data);
    }
}