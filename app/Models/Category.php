<?php

namespace App\Models;

class Category extends Model
{
    public $id;
    public $name;
    public $slug;
    public $meta_title;
    public $meta_description;
    public $parent_id;

    function __construct($news_data = [])
    {
        if (isset($news_data['id'])) {
            $this->id = $news_data['id'];
            $this->name = $news_data['name'];
            $this->slug = $news_data['slug'];
            $this->meta_title = $news_data['meta_title'];
            $this->meta_description = $news_data['meta_description'];
            $this->parent_id = $news_data['parent_id'];
        }
    }

    function showCategories($categories, $parent_id = 0, $char = '', $select = 0)
    {
        foreach ($categories as $key => $category) {
            $id = $category->id;
            $name = $category->name;
            if ($category->parent_id == $parent_id) {
                if ($id == $select) {
                    echo '<option value="' . $id . '" selected>' . $char . " " . $name . '</option>';
                } else {
                    echo '<option value="' . $id . '">' . $char . " " . $name . '</option>';
                }
                // Xóa chuyên mục đã lặp
                unset($categories[$key]);
                self::showCategories($categories, $id, $char . '|--', $select);
            }
        }
    }
}

?>