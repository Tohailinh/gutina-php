<?php
namespace App\Models;

class Login extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $fullname;
    public $avatar;
    public $status;

    function __construct($id, $username, $email, $password, $fullname, $avatar, $status)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->fullname = $fullname;
        $this->avatar = $avatar;
        $this->status = $status;
    }


    public static function _login($username, $password)
    {
        global $connect1;
        $sqlLogin = null;
        if ($username != null && $password != null) {
            $sql = "SELECT id, username, password, fullname, email, avatar FROM admin WHERE username = ? AND password = ? AND status = 1  ORDER BY id DESC LIMIT 0,1";
            $stmt = $connect1->prepare($sql);
            $stmt->bind_param("ss", $username, $password);
            $stmt->execute();
            $result = $stmt->get_result();
        }
        return $result;
    }

    public static function isLoginSessionExpired()
    {

        $login_session_duration = 3600;
        $current_time = time();
        if(isset($_SESSION['loggedin_time']) and isset($_SESSION["adminid"])){
            if((($current_time - $_SESSION['loggedin_time']) > $login_session_duration)){
                return true;
            }
        }
        return false;
    }
}
?>