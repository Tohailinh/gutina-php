<?php

namespace App\Models;

class Contract extends Model
{
    public $id;
    public $rank_salary;
    public $address;
    public $name;
    public $phone;
    public $email;

    function __construct($news_data = [])
    {
        if (isset($news_data['id'])) {
            $this->id = $news_data['id'];
            $this->rank_salary = $news_data['rank_salary'];
            $this->address = $news_data['address'];
            $this->name = $news_data['name'];
            $this->phone = $news_data['phone'];
            $this->email = $news_data['email'];
        }
    }
}

?>