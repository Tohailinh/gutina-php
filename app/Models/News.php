<?php

namespace App\Models;

class News extends Model
{
    public $id;
    public $avatar;
    public $name;
    public $slug;
    public $description;
    public $key_seo;
    public $content;
    public $status;
    public $category_id;
    public $views;
    public $date_publish;
    public $created_at;

    function __construct($news_data = [])
    {
        if (isset($news_data['id'])) {
            $this->id = $news_data['id'];
            $this->avatar = $news_data['avatar'];
            $this->name = $news_data['name'];
            $this->slug = $news_data['slug'];
            $this->description = $news_data['description'];
            $this->key_seo = $news_data['key_seo'];
            $this->content = $news_data['content'];
            $this->status = $news_data['status'];
            $this->category_id = $news_data['category_id'];
            $this->views = $news_data['views'];
            $this->date_publish = $news_data['date_publish'];
            $this->created_at = $news_data['created_at'];
        }
    }

    public static function categoryName($id)
    {
        global $connect1;
        $sqlCateName = null;
        $sql = "SELECT category.name as name_cate FROM news INNER JOIN category on news.category_id = category.id WHERE news.id = ?";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $sqlCateName = mysqli_fetch_array($result);
        if (empty($sqlCateName)) {
            return false;
        }
        return $sqlCateName;
    }

    public static function categoryAll()
    {
        global $connect1;
        $sqlCateAll = null;
        $sql = "SELECT id , name FROM category";
        $sqlCateAll = mysqli_query($connect1, $sql);
        if (empty($sqlCateAll)) {
            return false;
        }
        return $sqlCateAll;
    }

    public function loadCount($slug){
        global $connect1;
        $sqlCount = null;
        $sql = "SELECT news.id FROM news INNER JOIN category on news.category_id = category.id where category.slug = ?";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param("s", $slug);
        $stmt->execute();
        $result = $stmt->get_result();
        $sqlCount = mysqli_num_rows($result);
        if (empty($sqlCount)) {
            return false;
        }
        return $sqlCount;
    }

    public static function newsRandom($category_id)
    {
        global $connect1;
        $sqlCateAll = null;
        $sql = "SELECT * FROM news where category_id = ? order by rand() limit 3";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param("s", $category_id);
        $stmt->execute();
        $sqlCateAll = $stmt->get_result();
        if (empty($sqlCateAll)) {
            return false;
        }
        return $sqlCateAll;

    }

    public function newCate($where = [],$init = 0, $limit = 9){
        global $connect1;
        if ($where == ""){
            $where = 1;
        }
        $sql = " SELECT news.* FROM  news inner join category on news.category_id = category.id WHERE category.slug = '" . $where . "' LIMIT ". $init.",".$limit;
        $sqlNews = mysqli_query($connect1, $sql);

        foreach ($sqlNews as $item) {
            $list[] = new static($item);
        }
        return $list;
    }

    function trimmedTitle($title, $length = -1, $append = true)
    {

        if ($length == -1)
        {
            $length = 10;
        }

        if ($length)
        {
            $titlearr = preg_split('#(\r\n|\n|\r)#', $title);
            $title = '';
            $i = 0;
            foreach ($titlearr AS $key)
            {
                $title .= "$key \n";
                $i++;
                if ($i >= 10)
                {
                    break;
                }
            }
            $title = trim($title);
            unset($titlearr);
            if (strlen($title) > $length)
            {
                if (($pos = strrpos($title, ' ')) !== false)
                {

                    $title = substr($title, 0, 200);
                }
                if ($append)
                {
                    $title .= '...';
                }
            }
        }
        return $title;
    }

    public function findDetail($slug,$table)
    {
        global $connect1;
        $sqlNews = null;
        $sql = "SELECT * FROM " . $table . " WHERE slug = ?";
        $stmt = $connect1->prepare($sql);
        $stmt->bind_param("s", $slug);
        $stmt->execute();
        $result = $stmt->get_result();
        $sqlNews = mysqli_fetch_array($result);

        if (empty($sqlNews)) {
            return false;
        }
        return new static($sqlNews);
    }



}

?>