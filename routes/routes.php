<?php
/**
 * Created by PhpStorm.
 * User: Linhth
 * Date: 12/10/2018
 * Time: 2:21 PM
 */

use App\Models\Login;

session_start();
//settimeout session login
if(isset($_SESSION["adminid"])) {
    if(Login::isLoginSessionExpired()) {
        header("Location:/admin/Login/logout");
    }
}

if (!empty($_SESSION['request'])){
    $old = $_SESSION['request'];
}

$controllers = [
    'Admin' => ['index', 'store', 'create', 'edit', 'update', 'delete', 'show', 'itemActions'],
    'Category' => ['index', 'store', 'create', 'edit', 'update','delete', 'show', 'itemActions'],
    'Customers' => ['index', 'store', 'create', 'edit', 'update', 'delete', 'show', 'itemActions'],
    'News' => ['index', 'store', 'create', 'edit', 'update','delete', 'show', 'itemActions'],
    'filemanager' => ['dialog.php'],
    'Contract' => ['index', 'delete', 'show', 'itemActions'],
    'Menu' => ['index', 'show', 'create', 'edit', 'itemActions'],
    'Login' => ['login', 'logout'],
    'Pages' => ['error'],
    'Home' => ['index', 'lienHe', 'hoiDap', 'rutTien', 'nopTien', 'chiTietTinTuc', 'doiTac'],
]; // Các controllers trong hệ thống và các action có thể gọi ra từ controller đó.
// Nếu các tham số nhận được từ URL không hợp lệ (không thuộc list controller và action có thể gọi
// thì trang báo lỗi sẽ được gọi ra.

$action_arr = explode('?', $action);
$action = $action_arr[0];
if ($controller == '') {
    if ($controller == '') {
        $controller = 'Home';
        $action = 'index';
    }
} else if ($admin != 'admin' && $controller) {
    if ($controller == 'tin-tuc' && $action != 'index') {
        $controller = 'Home';
        $action = 'chiTietTinTuc';
    } else {
        //router fontend
        switch ($controller) {
            case'lien-he':
                $controller = 'Home';
                $action = 'lienHe';
                break;
            case'hoi-dap':
                $controller = 'Home';
                $action = 'hoiDap';
                break;
            case'rut-tien':
                $controller = 'Home';
                $action = 'rutTien';
                break;
            case'nop-tien':
                $controller = 'Home';
                $action = 'nopTien';
                break;
            case'tin-tuc':
                $controller = 'Home';
                $action = 'tinTuc';
                break;
            case'doi-tac':
                $controller = 'Home';
                $action = 'doiTac';
                break;
            default:
                $controller = 'Home';
                $action = 'tinTuc';
                break;
        }
    }
} elseif ($admin != 'admin' || !array_key_exists($controller, $controllers) || !in_array($action_arr[0], $controllers[$controller])) {
    $controller = 'Pages';
    $action = 'error';
    header('Location: /admin/Pages/error');die;
}

if (!isset($_SESSION['login']) && $admin == 'admin') {
    if ($controller != 'Pages' && $action != 'error') {
        if ($admin == 'admin' && $controller == 'Login' && $action == 'login') {
            $controller = 'Login';
            $action = 'login';
        } else {
            header('Location: /admin/Login/login');
            die;
        }
    }
} elseif ($admin == 'admin') {
    if ($controller == 'Login' && $action == 'login'){
        $controller = 'Admin';
        $action = 'index';
        header('Location: /admin/Admin/index');
    }elseif ($controller == 'Home' && $action == 'index'){
        header('Location: /admin/Admin/index');
    }

}

// Tạo ra tên controller class từ các giá trị lấy được từ URL sau đó gọi ra để hiển thị trả về cho người dùng.
$class = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
$app = 'App\Controllers\\' . $class;
$controller = new $app;
$controller->$action();

