<?php
/**
 * Created by PhpStorm.
 * User: AoE_Test
 * Date: 12/10/2018
 * Time: 4:51 PM
 */

function my_app_autoloader($class)
{
    $root = 'app/';
    $prefix = 'App\\';

    $classWithoutPrefix = preg_replace('/^' . preg_quote($prefix,'') . '/', '', $class);
//    var_dump($class);
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $classWithoutPrefix) . '.php';
    $path = '../'.$root . $file;
    if (file_exists($path)) {
        require_once $path;
    }
}
spl_autoload_register('my_app_autoloader');