<?php

require_once '../vendor/autoload.php';
$path = substr($_SERVER['DOCUMENT_ROOT'],0, -7);
$dotenv = Dotenv\Dotenv::create($path);
$path_env = $path.'/.env';
if(file_exists($path_env)) {
    $dotenv->load();
}

