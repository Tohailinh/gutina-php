<?php
use App\Models\Category;
if (!empty($_SESSION['request'])){
    $old = $_SESSION['request'];
}
?>

<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">

    <!-- Tên danh mục Field -->
    <div class="form-group col-sm-12">
        <label for="name">Tên danh mục :</label>
        <span class="text-danger">(*)</span>
        <input placeholder="Tên danh mục" class="form-control" name="name" type="text"
               id="name" value="<?php  echo !empty($old) ? $old['name'] : $antiXss->xss_clean($category->name); ?>" required  onkeyup="ChangeToSlug();">
    </div>

    <div class="form-group col-sm-12">
        <label for="rate">Danh mục cha :</label>
        <select name="parent_id" id="" class="form-control">
            <option value='0'>Danh mục cha</option>
            <?php
                $categories = Category::readAll('category', '');
                Category::showCategories($categories, 0, '', $category->parent_id);
            ?>
        </select>
    </div>

    <!-- Tiêu đề Field -->
    <div class="form-group col-sm-12">
        <label for="meta_title">Tiêu đề :</label>
        <span class="text-danger">(*)</span>
        <input placeholder="Tiêu đề" class="form-control" name="meta_title" type="text"
               id="meta_title" required value="<?php echo !empty($old) ? $old['name'] : $antiXss->xss_clean($category->meta_title); ?>">
    </div>

    <!-- slug Field -->


    <div class="form-group col-sm-12">
        <label for="slug">Slug :</label>
        <span class="text-danger">(*)</span>
        <input placeholder="slug" class="form-control" name="slug" type="text" required
               id="slug" value="<?php echo $antiXss->xss_clean($category->slug) ?>" >
    </div>

    <!-- slug Field -->
    <div class="form-group col-sm-12">
        <label for="meta_description">Mô tả :</label>
        <textarea placeholder="Mô tả" class="form-control" name="meta_description" rows="4" cols="50"
                  id="slug"><?php echo !empty($old) ? $old['name'] : $antiXss->xss_clean($category->meta_description); ?></textarea>
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">
    <a href="/admin/Category/index" class="btn btn-default">Thoát </a>

    <input class="btn btn-success" type="submit" value="Lưu" name="submit">
</div>
<div class="clearfix"></div>

<?php
unset($_SESSION['request']);
?>