<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Cập nhật
        <small><?= $antiXss->xss_clean($category->name) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/Category/index">Danh sách danh mục</a></li>
        <li class="active">Cập nhật "<?= $antiXss->xss_clean($category->name) ?>" </li>
    </ol>
</section>
<div class="content">
    <?php include('../Views/layouts/error.php') ?>
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="/admin/Category/update/?id=<?= $category->id ?>" class="form jsvalidation" name="admin_create" enctype="multipart/form-data">
                    <?php include '../Views/category/fields.php' ?>
                </form>
            </div>
        </div>
    </div>
</div>

