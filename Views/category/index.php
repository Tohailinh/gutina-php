<?php
use App\Models\Category;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Danh sách danh mục
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/Category/index"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Danh sách danh mục</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <input id="data-link-item-actions" value="/admin/Category/itemActions" hidden>
            <form action="" method="get" id="form_custom_list">
                <div class="div_option">
                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="form-group">
                            <div class="col-sm-10 col-xs-12 input-group">
                                <input id="name" class="form-control search_name enter-submit" name="name" placeholder="Tìm kiếm theo tên danh mục ..." value="<?= $name ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="div_option wrap-filters ">
                            <button class="btn btn-success form-group" type="submit">Tìm kiếm </button>
                            <a class="btn btn-default" href="/admin/Category/index">Clear</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="div_option form-group mg-top-10">

                    <div class="col-xs-3 col-md-6 col-sm-6">
                    </div>

                    <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                        <a href="/admin/Category/create" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới danh mục
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

            <div class="box">
                <?php include('../Views/layouts/message.php') ?>
                <?php include('../Views/layouts/error.php') ?>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên danh mục</th>
                            <th>Danh mục cha</th>
                            <th>Tiêu đề</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($categories as $v => $category) { ?>
                            <tr>
                                <td></td>
                                <td><?php echo $antiXss->xss_clean($category->name) ?></td>
                                <?php
                                    $id = $category->id;
                                    $name = $category->name;
                                    $parent_id = $category->parent_id;
                                    $cate = Category::find($parent_id,'category');
                                    if (!empty($cate)){
                                        $name = '|---'.$cate->name;
                                    }else{
                                        $name = 'Danh mục cha';
                                    }
                                ?>

                                <td><?php echo $name ?></td>
                                <td><?php echo $antiXss->xss_clean($category->meta_title) ?></td>
                                <td>
                                    <div class='btn-group'>
                                        <a href="/admin/Category/show/?id=<?php echo $category->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <a href="/admin/Category/edit/?id=<?php echo $category->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                                           data-link="data-link-item-actions" data-key="xóa" data-title="danh mục" data-text="xóa"
                                           data-val="none" data-id="<?php echo $category->id ?>">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                        <input type="hidden" name="data-id" id="data-id" value="">
                                    </div>
                                </td>

                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

