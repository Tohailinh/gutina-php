<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Cập nhật
        <small><?= $antiXss->xss_clean($admin->fullname) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/Admin/index">Danh sách quản trị viên</a></li>
        <li><a href="/admin/Admin/edit?id=<?= $admin->id ?>">Cập nhật "<?= $antiXss->xss_clean($admin->fullname) ?>"</a></li>
    </ol>
</section>

<div class="content">
    <?php include('../Views/layouts/error.php') ?>
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="/admin/Admin/update/?id=<?= $admin->id ?>" class="form jsvalidation" name="admin_create" enctype="multipart/form-data">
                    <?php include '../Views/admin/fields.php' ?>
                </form>
            </div>
        </div>
    </div>
</div>

