<?php
if (!empty($_SESSION['request'])){
    $old = $_SESSION['request'];
}
?>
<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
        <img id="img-upload" class="img-fluid center-block" width="200px" height="200px">
        <div id="img-upload-remove">
            <img src="<?= !empty($antiXss->xss_clean($admin->avatar)) ? '/uploads/users' . '/' . $antiXss->xss_clean($admin->avatar) : '/uploads/default/no-image.jpg' ?>"
                 class="img-fluid img-thumbnail center-block " width="200px" height="200px">
        </div>
    </div>

    <div class="form-group  col-md-12 col-sm-12 col-xs-12 text-center">
        <label for="image" class="font-weight-bold float-left mr-4">Ảnh <span class="required_data">*</span></label>
        <input class="form-control w-75" accept="image/*" name="image" placeholder="Ảnh" id="image" type="file">
    </div>

    <!-- Fullname Field -->
    <div class="form-group col-sm-12">
        <label for="fullname">Tên đầy đủ :</label>
        <input placeholder="Tên đầy đủ" class="form-control" name="fullname" type="text" id="fullname" value="<?php
        echo !empty($old) ? $old['fullname'] : $antiXss->xss_clean($admin->fullname);
        ?>">
    </div>

    <!-- Username Field -->
    <div class="form-group col-sm-12">
        <label for="username">Tài khoản :</label>
        <span class="text-danger">(*)</span>
        <input placeholder="Tài khoản" class="form-control" name="username" type="text" id="username" required value="<?php
        echo !empty($old) ? $old['username'] : $antiXss->xss_clean($admin->username);
        ?>"<?php echo !empty($antiXss->xss_clean($admin->username)) ? 'readonly' : '' ?>>
    </div>

    <?php if ($_SESSION['check_update']) { ?>

        <div class="form-group col-sm-12">
            <label for="password">Mật khẩu mới:</label>
            <input type="checkbox" name="changepassword" id="changepassword" title="">
            <input class="form-control password" name="password" placeholder="Mật khẩu mới" id="up_admin_password" type="password">
        </div>
    <?php } else { ?>
        <!-- Password Field -->
        <div class="form-group col-sm-12">
            <label for="password">Mật khẩu:</label>
            <span class="text-danger">(*)</span>
            <input class="form-control" placeholder="Mật khẩu" name="password" type="password"
                   value="<?php echo $antiXss->xss_clean($admin->password) ?>" id="password" required>
        </div>

    <?php } ?>
    <!-- Email Field -->
    <div class="form-group col-sm-12">
        <label for="email">Email:</label>
        <span class="text-danger">(*)</span>
        <input placeholder="Email" class="form-control" id="email" name="email" type="email" required value="<?php
        echo !empty($old) ? $old['email'] : $antiXss->xss_clean($admin->email);
        ?>"<?php echo !empty($admin->email) ? 'readonly' : '' ?>>
    </div>

    <!-- status Field -->
    <div class="form-group col-sm-12 col-xs-12 col-md-12">
        <label for="activated">Trạng thái :</label>
        <div class="material-switch float-left" style="margin-top: 10px">
            <input name="status" type="hidden" value="">
            <?php
            $array_status = [];
            if ($admin != '') {
                if ($admin->status == 1) {
                    $array_status['checked'] = 'checked';
                }
            } else {
                $array_status['checked'] = 'checked';
            }
            ?>
            <input id="someSwitchOptionSuccess" <?php echo $array_status['checked'] ?> name="status" type="checkbox"
                   value="1">
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">
    <a href="/admin/Admin/index" class="btn btn-default">Thoát </a>

    <input class="btn btn-success" type="submit" value="Lưu" name="submit">
</div>
<div class="clearfix"></div>

<?php
unset($_SESSION['request']);
?>