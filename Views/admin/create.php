<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Thêm mới quản trị viên
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/Admin/index">Danh sách quản trị viên</a></li>
        <li class="active">Thêm mới quản trị viên</li>
    </ol>
</section>
<div class="content">
    <?php include('../Views/layouts/error.php') ?>
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="/admin/Admin/store" class="form jsvalidation" name="admin_create" enctype="multipart/form-data">
                    <?php require_once '../Views/admin/fields.php' ?>
                </form>
            </div>
        </div>
    </div>
</div>
