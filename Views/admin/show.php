<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chi tiết
        <small><??></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="/admin/Admin/index">quản lý quản trị viên</a></li>
        <li class="active"><a href="/admin/Admin/show/?id=<?= $admin->id ?>">Chi tiết quản trị <?= $admin->id ?></a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-offset-2 col-sm-offset-2 col-xs-12 col-sm-6 col-md-6">
            <div class="box">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?= !empty($antiXss->xss_clean($admin->avatar)) ? '/uploads/users' . '/' .$antiXss->xss_clean($admin->avatar) : '/uploads/default/no-image.jpg'   ?>" alt="User profile picture">
                        <h2 class="profile-username text-center"><strong><?= $antiXss->xss_clean($admin->fullname) ?></strong></h2>

                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 20%">Tên đầy đủ</th>
                                <td><?= $antiXss->xss_clean($admin->fullname) ?></td>
                            </tr>

                            <tr>
                                <th style="width: 20%">Tài khoản</th>
                                <td><?= $antiXss->xss_clean($admin->username) ?></td>
                            </tr>

                            <tr>
                                <th>Email</th>
                                <td><?= $antiXss->xss_clean($admin->email) ?></td>
                            </tr>

                            <tr>
                                <th>Trạng thái</th>
                                <td>
                                    <?php if($admin->status == 1){ ?>
                                        <button type="button" class="btn_status btn_status_success item_actions">Kích hoạt</button>
                                    <?php }else{ ?>
                                        <button type="button" class="btn_status btn_status_false item_actions">Vô hiệu</button>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="form-group col-sm-12 col-xs-12 row text-center">
                            <a href="/admin/Admin/index" class="btn btn-default">Thoát </a>
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

