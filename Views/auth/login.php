
<div class="login-box">
  <div class="login-logo">
      <img src="../../assets/img/gutina.png" alt="">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
      <?php include('../Views/layouts/error.php')?>
      <form action="login" method="post" name="Login_Form" class="form-signin">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Tài khoản" name="username" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Mật khẩu" name="password" required >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
        <div class="row">
            <div class="col-xs-8">
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="Submit" class="btn btn-primary btn-block btn-flat" name="login" value="login">Đăng nhập</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!---->
