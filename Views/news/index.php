<?php
use App\Models\News;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Danh sách tin tức
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/News/index"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Danh sách tin tức</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <input id="data-link-item-actions" value="/admin/News/itemActions" hidden>
            <form action="" method="get" id="form_custom_list">
                <div class="div_option">
                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="form-group">
                            <div class="col-sm-10 col-xs-12 input-group">
                                <input id="new_name" class="form-control search_name enter-submit" name="name" placeholder="Tìm kiếm theo tên tin tức ..." value="<?= $name ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="form-group">
                            <div class="col-sm-10 col-xs-12 input-group">
                                <select name="filter_status" class="form-control filter_header" id="status">
                                    <option value="none" <?= ($filterStatus == 'none') ? 'selected="selected"' : '' ?> selected="selected">Trạng thái </option>
                                    <option value="1" <?= ($filterStatus == '1') ? 'selected="selected"' : '' ?>>Kích hoạt</option>
                                    <option value="0" <?= ($filterStatus == '0') ? 'selected="selected"' : '' ?>>Vô hiệu </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="div_option wrap-filters ">
                            <button class="btn btn-success form-group" type="submit">Tìm kiếm </button>
                            <a class="btn btn-default" href="/admin/News/index">Clear</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="div_option form-group mg-top-10">

                    <div class="col-xs-3 col-md-6 col-sm-6">
                    </div>

                    <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                        <a href="/admin/News/create" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới tin tức
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <div class="box">
                <?php include('../Views/layouts/message.php') ?>
                <?php include('../Views/layouts/error.php') ?>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tiêu đề</th>
                            <th>Danh mục</th>
                            <th>Lượt xem</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($news as $v => $new) {
                            $cate_name = News::categoryName($new->id);
                            if (!empty($cate_name)){
                                $name_cate = $cate_name['name_cate'];
                            }else{
                                $name_cate = '';
                            }
                            ?>
                            <tr>
                                <td></td>
                                <td><?= $antiXss->xss_clean($new->name) ?></td>
                                <td><?= $antiXss->xss_clean($name_cate) ?></td>
                                <td><?= $antiXss->xss_clean($new->views) ?></td>
                                <td>
                                    <?php if($new->status == 1){ ?>
                                        <button type="button" class="btn_status btn_status_success item_actions">Kích hoạt</button>
                                    <?php }else{ ?>
                                        <button type="button" class="btn_status btn_status_false item_actions">Vô hiệu</button>
                                    <?php } ?>
                                </td>
                                <td>
                                    <div class='btn-group'>
                                        <a href="/admin/News/show/?id=<?php echo $new->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <a href="/admin/News/edit/?id=<?php echo $new->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                                           data-link="data-link-item-actions" data-key="xóa" data-title="tin tức" data-text="xóa"
                                           data-val="none" data-id="<?php echo $new->id ?>">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                        <input type="hidden" name="data-id" id="data-id" value="">
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

