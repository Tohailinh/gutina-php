<?php
use App\Models\Category;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chi tiết tin tức
        <small><?= $news->customer_name ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="/admin/News/index">Danh sách khách hàng</a></li>
        <li class="active"><a href="/admin/News/show/?id=<?= $news->id ?>">Chi tiết tin tức <?= $news->name ?></a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-offset-2 col-sm-offset-2 col-xs-12 col-sm-6 col-md-6">
            <div class="box">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle"
                             src="<?= !empty($antiXss->xss_clean($news->avatar)) ? '/uploads/news' . '/' . $antiXss->xss_clean($news->avatar) : '/uploads/default/no-image.jpg' ?>"
                             alt="User profile picture">
                        <h2 class="profile-username text-center"><strong><?= $antiXss->xss_clean($news->customer_name) ?></strong></h2>
                        <table class="table">
                            <tbody  style="width: 100%; height: 50px!important; ;overflow: scroll">
                            <tr>
                                <th style="width: 20%">Tiêu đề</th>
                                <td><?= $antiXss->xss_clean($news->name) ?></td>
                            </tr>

                            <tr>
                                <th>Danh mục</th>
                                <?php
                                    $category = Category::find($news->category_id,'category');
                                ?>
                                <td><?= $antiXss->xss_clean($category->name) ?></td>
                            </tr>

                            <tr>
                                <th>Slug</th>
                                <td><?= $antiXss->xss_clean($news->slug) ?></td>
                            </tr>

                            <tr>
                                <th>Nhập tóm tắt</th>
                                <td><?= $antiXss->xss_clean($news->description) ?></td>
                            </tr>

                            <tr>
                                <th>Key seo</th>
                                <td><?= $antiXss->xss_clean($news->key_seo) ?></td>
                            </tr>

                            <tr>
                                <th>Mô tả</th>
                                <td><?= $antiXss->xss_clean($news->content) ?></td>
                            </tr>

                            <tr>
                                <th>Trạng thái</th>
                                <td>
                                    <?php if ($news->status == 1) { ?>
                                        <button type="button" class="btn_status btn_status_success item_actions">Kích
                                            hoạt
                                        </button>
                                    <?php } else { ?>
                                        <button type="button" class="btn_status btn_status_false item_actions">Vô hiệu
                                        </button>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="form-group col-sm-12 col-xs-12 row text-center">
                            <a href="/admin/News/index" class="btn btn-default">Thoát </a>
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

