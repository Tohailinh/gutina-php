<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Thêm mới tin tức
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/News/index">Danh sách tin tức</a></li>
        <li class="active">Thêm mới tin tức</li>
    </ol>
</section>
<div class="content">
    <?php include('../Views/layouts/error.php') ?>
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="/admin/News/store" class="form jsvalidation" name="news_create" enctype="multipart/form-data">
                    <?php require_once '../Views/news/fields.php' ?>
                </form>
            </div>
        </div>
    </div>
</div>
