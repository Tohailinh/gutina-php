<?php
use App\Models\Category;
if (!empty($_SESSION['request'])){
    $old = $_SESSION['request'];
}
?>

<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
        <img id="img-upload" class="img-fluid center-block" width="200px" height="200px">
        <div id="img-upload-remove">
            <img src="<?= !empty($antiXss->xss_clean($news->avatar)) ? '/uploads/news' . '/' . $antiXss->xss_clean($news->avatar) : '/uploads/default/no-image.jpg' ?>"
                 class="img-fluid img-thumbnail center-block " width="200px" height="200px">
        </div>
    </div>

    <div class="form-group  col-md-12 col-sm-12 col-xs-12 text-center">
        <label for="image" class="font-weight-bold float-left mr-4">Ảnh <span class="_data">*</span></label>
        <input class="form-control w-75" accept="image/*" name="image" placeholder="Ảnh" id="image"
               type="file">
        <!--</div>-->
    </div>

    <!-- Tên khách hàng Field -->
    <div class="form-group col-sm-6">
        <label for="fullname">Tiêu đề :</label>
        <span class="text-danger">(*)</span>
        <input placeholder="Tiêu đề" class="form-control" name="name" type="text"
               id="name" value="<?php echo $antiXss->xss_clean($news->name) ?>"  onkeyup="ChangeToSlug();" required>
    </div>

    <div class="form-group col-sm-6 col-xs-12 col-md-6">
        <label for="rate">Danh mục :</label>
        <select class="form-control" data-placeholder="Danh mục" style="width: 100%;" name="category_id">
            <?php
            $categories = Category::readAll('category', '');
            Category::showCategories($categories, 0, '', $news->category_id);
            ?>
        </select>
    </div>

    <!-- Slug Field -->
    <div class="form-group col-sm-6 col-xs-12 col-md-6">
        <label for="username">Slug :</label>
        <input placeholder="slug" class="form-control" name="slug" type="text"
               id="slug" value="<?php echo $antiXss->xss_clean($news->slug) ?>" >
    </div>

    <!-- Tiêu đề Field -->
    <div class="form-group col-sm-6 col-xs-12 col-md-6">
        <label for="email">Nhập tóm tắt :</label>
        <input placeholder="Tiêu đề" class="form-control" id="description" name="description"
               type="text"
               value="<?php echo $antiXss->xss_clean($news->description) ?>">
    </div>


    <!-- Tiêu đề Field -->
    <div class="form-group col-sm-6 col-xs-12 col-md-6">
        <label for="email">Key seo :</label>
        <input placeholder="Key seo" class="form-control" id="key_seo" name="key_seo"
               type="text" value="<?php echo $antiXss->xss_clean($news->key_seo) ?>">
    </div>


    <!-- status Field -->
    <div class="form-group col-sm-6 col-xs-12 col-md-6">
        <label for="activated">Trạng thái :</label>
        <div class="material-switch float-left" style="margin-top: 10px">
            <input name="status" type="hidden" value="">
            <?php
            $array_status = [];
            if ($news != '') {
                if ($news->status == 1) {
                    $array_status['checked'] = 'checked';
                }
            } else {
                $array_status['checked'] = 'checked';
            }
            ?>
            <input id="someSwitchOptionSuccess" <?php echo $array_status['checked'] ?> name="status"
                   type="checkbox" value="1">
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>
    </div>


    <div class="orm-group col-sm-12 pad">
        <label for="content">Mô tả :</label>
        <textarea id="content-selector" name="content">
                                <?php echo $news->content ?>
                            </textarea>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">
    <a href="/admin/News/index" class="btn btn-default">Thoát </a>

    <input class="btn btn-success" type="submit" value="Lưu" name="submit">
</div>
<div class="clearfix"></div>
<?php
unset($_SESSION['request']);
?>
<script>
    function ChangeToSlug() {
        var title, slug;
        //Lấy text từ thẻ input title
        str = document.getElementById("name");
        if (str != null) {
            title = str.value;
        }
        else {
            title = null;
        }

        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();

        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        document.getElementById('slug').value = slug;
    }

</script>