<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Cập nhật
        <small><?= $antiXss->xss_clean($customers->customer_name) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/Customers/index">Danh sách quản trị viên</a></li>
        <li><a href="/admin/Customers/edit?id=<?= $customers->id ?>">Cập nhật "<?= $antiXss->xss_clean($customers->customer_name) ?>"</a></li>
    </ol>
</section>
<div class="content">
    <?php include('../Views/layouts/error.php') ?>
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="/admin/News/store" class="form jsvalidation" name="news_create" enctype="multipart/form-data">
                    <?php include '../Views/news/fields.php' ?>
                </form>
            </div>
        </div>
    </div>
</div>
