<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Danh sách khách hàng
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/Customers/index"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Danh sách khách hàng</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <input id="data-link-item-actions" value="/admin/Customers/itemActions" hidden>
            <form action="" method="get" id="form_custom_list">
                <div class="div_option">
                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="form-group">
                            <div class="col-sm-10 col-xs-12 input-group">
                                <input id="customer_name" class="form-control search_name enter-submit" name="customer_name" placeholder="Tìm kiếm theo tên khách hàng ..." value="<?= $customer_name ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="form-group">
                            <div class="col-sm-10 col-xs-12 input-group">
                                <select name="filter_status" class="form-control filter_header" id="status">
                                    <option value="none" <?= ($filterStatus == 'none') ? 'selected="selected"' : '' ?> selected="selected">Trạng thái </option>
                                    <option value="1" <?= ($filterStatus == '1') ? 'selected="selected"' : '' ?>>Kích hoạt</option>
                                    <option value="0" <?= ($filterStatus == '0') ? 'selected="selected"' : '' ?>>Vô hiệu </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="div_option wrap-filters ">
                            <button class="btn btn-success form-group" type="submit">Tìm kiếm </button>
                            <a class="btn btn-default" href="/admin/Customers/index">Clear</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="div_option form-group mg-top-10">

                    <div class="col-xs-3 col-md-6 col-sm-6">
                    </div>

                    <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                        <a href="/admin/Customers/create" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới khách hàng
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
            <div class="box">
                <?php include('../Views/layouts/message.php') ?>
                <?php include('../Views/layouts/error.php') ?>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên khách hàng</th>
                            <th>Nghề nghiệp</th>
                            <th>Tiêu đề</th>
                            <th>Chọn số sao</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($customers as $v => $customer) { ?>
                            <tr>
                                <td></td>
                                <td><?php echo $antiXss->xss_clean($customer->customer_name) ?></td>
                                <td><?php echo $antiXss->xss_clean($customer->job) ?></td>
                                <td><?php echo $antiXss->xss_clean($customer->title) ?></td>
                                <td><?php echo $customer->rate ?></td>
                                <td>
                                    <?php if($customer->status == 1){ ?>
                                        <button type="button" class="btn_status btn_status_success item_actions">Kích hoạt</button>
                                    <?php }else{ ?>
                                        <button type="button" class="btn_status btn_status_false item_actions">Vô hiệu</button>
                                    <?php } ?>
                                </td>
                                <td>
                                    <div class='btn-group'>
                                        <a href="/admin/Customers/show/?id=<?php echo $customer->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <a href="/admin/Customers/edit/?id=<?php echo $customer->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                                           data-link="data-link-item-actions" data-key="xóa" data-title="khách hàng" data-text="xóa"
                                           data-val="none" data-id="<?php echo $customer->id ?>">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                        <input type="hidden" name="data-id" id="data-id" value="">
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

