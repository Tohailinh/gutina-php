<?php
if (!empty($_SESSION['request'])){
    $old = $_SESSION['request'];
}
?>

<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
        <img id="img-upload" class="img-fluid center-block" width="200px" height="200px">
        <div id="img-upload-remove">
            <img src="<?= !empty($customers->avatar) ? '/uploads/customers' . '/' . $customers->avatar : '/uploads/default/no-image.jpg' ?>"
                 class="img-fluid img-thumbnail center-block " width="200px" height="200px">
        </div>
    </div>

    <div class="form-group  col-md-12 col-sm-12 col-xs-12 text-center">
        <label for="image" class="font-weight-bold float-left mr-4">Ảnh <span class="required_data">*</span></label>
        <input class="form-control w-75" accept="image/*" name="image" placeholder="Ảnh" id="image" type="file">
        <!--                        </div>-->
    </div>
    <!-- Tên khách hàng Field -->
    <div class="form-group col-sm-12">
        <label for="fullname">Tên khách hàng :</label>
        <span class="text-danger">(*)</span>
        <input placeholder="Tên khách hàng" class="form-control" name="customer_name" type="text"
               id="customer_name" value="<?php echo !empty($old) ? $old['customer_name'] : $antiXss->xss_clean($customers->customer_name); ?>" required>
    </div>

    <!-- Nghề nghiệp Field -->
    <div class="form-group col-sm-12">
        <label for="username">Nghề nghiệp :</label>
        <input placeholder="Nghề nghiệp" class="form-control" name="job" type="text"
               id="job" value="<?php echo !empty($old) ? $old['job'] : $antiXss->xss_clean($customers->job); ?>" >
    </div>

    <!-- Tiêu đề Field -->
    <div class="form-group col-sm-12">
        <label for="email">Tiêu đề:</label>
        <input placeholder="Tiêu đề" class="form-control" id="title" name="title" type="text"
               value="<?php echo !empty($old) ? $old['title'] : $antiXss->xss_clean($customers->title); ?>" >
    </div>

    <div class="form-group col-sm-12">
        <label for="rate">Chọn số sao:</label>
        <select class="form-control" data-placeholder="Chọn số sao" style="width: 100%;" name="rate">
            <?php
            for ($i = 1; $i <= 5; $i++) {
                ?>
                <option value="<?= $i ?>" <?php if ($customers->rate == $i) { ?> selected <?php } ?>><?= $i ?></option>
                <?php
            }
            ?>
        </select>
    </div>

    <!-- Tiêu đề Field -->
    <div class="form-group col-sm-12">
        <label for="content">Mô tả :</label>
        <textarea placeholder="Tiêu đề" class="form-control" id="content" name="content" rows="6">
            <?php echo !empty($old) ? $old['content'] : $antiXss->xss_clean($customers->content); ?>
        </textarea>
    </div>


    <!-- status Field -->
    <div class="form-group col-sm-12 col-xs-12 col-md-12">
        <label for="activated">Trạng thái :</label>
        <div class="material-switch float-left" style="margin-top: 10px">
            <input name="status" type="hidden" value="">
            <?php
            $array_status = [];
            if ($customers != '') {
                if ($customers->status == 1) {
                    $array_status['checked'] = 'checked';
                }
            } else {
                $array_status['checked'] = 'checked';
            }
            ?>
            <input id="someSwitchOptionSuccess" <?php echo $array_status['checked'] ?> name="status"
                   type="checkbox" value="1">
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">
    <a href="/admin/Customers/index" class="btn btn-default">Thoát </a>

    <input class="btn btn-success" type="submit" value="Lưu" name="submit">
</div>
<div class="clearfix"></div>

<?php
    unset($_SESSION['request']);
?>