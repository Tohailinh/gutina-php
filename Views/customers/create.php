<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Thêm mới khách hàng
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/Customers/index">Danh sách khách hàng</a></li>
        <li class="active">Thêm mới khách hàng</li>
    </ol>
</section>
<div class="content">
    <?php include('../Views/layouts/error.php') ?>

    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="/admin/Customers/store" class="form jsvalidation" name="customer_create" enctype="multipart/form-data">
                    <?php require_once '../Views/customers/fields.php' ?>
                </form>
            </div>
        </div>
    </div>
</div>
