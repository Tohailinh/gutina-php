<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chi tiết khách hàng
        <small><?= $antiXss->xss_clean($customers->customer_name) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="/admin/Customers/index">Danh sách khách hàng</a></li>
        <li class="active"><a href="/admin/Customers/show/?id=<?= $antiXss->xss_clean($customers->customer_name) ?>">Chi tiết khách hàng <?= $antiXss->xss_clean($customers->customer_name) ?></a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-offset-2 col-sm-offset-2 col-xs-12 col-sm-6 col-md-6">
            <div class="box">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?= !empty($antiXss->xss_clean($customers->avatar)) ? '/uploads/customers' . '/' . $antiXss->xss_clean($customers->avatar) : '/uploads/default/no-image.jpg'   ?>" alt="User profile picture">
                        <h2 class="profile-username text-center"><strong><?= $antiXss->xss_clean($customers->customer_name) ?></strong></h2>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 20%">Nghề nghiệp</th>
                                <td><?= $antiXss->xss_clean($customers->job) ?></td>
                            </tr>

                            <tr>
                                <th>Tiêu đề</th>
                                <td><?= $antiXss->xss_clean($customers->title) ?></td>
                            </tr>

                            <tr>
                                <th>Số sao</th>
                                <td><?= $customers->rate ?></td>
                            </tr>

                            <tr>
                                <th>Nội dung</th>
                                <td><?= $antiXss->xss_clean($customers->content) ?></td>
                            </tr>

                            <tr>
                                <th>Trạng thái</th>
                                <td>
                                    <?php if($customers->status == 1){ ?>
                                        <button type="button" class="btn_status btn_status_success item_actions">Kích hoạt</button>
                                    <?php }else{ ?>
                                        <button type="button" class="btn_status btn_status_false item_actions">Vô hiệu</button>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="form-group col-sm-12 col-xs-12 row text-center">
                            <a href="/admin/Customers/index" class="btn btn-default">Thoát </a>
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

