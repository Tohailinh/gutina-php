<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Danh sách đối tác
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/Contract/index"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Danh sách đối tác</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <input id="data-link-item-actions" value="/admin/Contract/itemActions" hidden>
            <form action="" method="get" id="form_custom_list">
                <div class="div_option">
                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="form-group">
                            <div class="col-sm-10 col-xs-12 input-group">
                                <input id="name" class="form-control search_name enter-submit" name="name" placeholder="Tìm kiếm theo họ và tên ..." value="<?= $name ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 ">
                        <div class="div_option wrap-filters ">
                            <button class="btn btn-success form-group" type="submit">Tìm kiếm </button>
                            <a class="btn btn-default" href="/admin/Contract/index">Clear</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="div_option form-group mg-top-10">

                    <div class="col-xs-3 col-md-6 col-sm-6">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

            <div class="box">
                <?php include('../Views/layouts/message.php') ?>
                <?php include('../Views/layouts/error.php') ?>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Khoảng thu nhập</th>
                            <th>Khu vực muốn hợp tác</th>
                            <th>Họ và tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($contracts as $v => $contract) { ?>
                            <tr>
                                <td></td>
                                <td><?php echo $contract->rank_salary ?></td>
                                <td><?php echo $antiXss->xss_clean($contract->address) ?></td>
                                <td><?php echo $antiXss->xss_clean($contract->name) ?></td>
                                <td><?php echo $antiXss->xss_clean($contract->phone) ?></td>
                                <td><?php echo $contract->email ?></td>
                                <td>
                                    <div class='btn-group'>
                                        <a href="/admin/Contract/show/?id=<?php echo $contract->id ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <input type="hidden" name="data-id" id="data-id" value="">
                                        <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                                           data-link="data-link-item-actions" data-key="xóa" data-title="đối tác" data-text="xóa"
                                           data-val="none" data-id="<?php echo $contract->id ?>">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                    </div>
                                </td>

                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

