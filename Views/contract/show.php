<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Chi tiết đối tác
        <small><?php echo $antiXss->xss_clean($contract->name) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="/admin/Contract/index">Danh sách đối tác</a></li>
        <li class="active"><a href="/admin/Contract/show/?id=<?= $contract->name ?>">Chi tiết danh mục "<?= $antiXss->xss_clean($contract->name) ?>"</a>
        </li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-offset-2 col-sm-offset-2 col-xs-12 col-sm-6 col-md-6">
            <?php include('../Views/layouts/error.php') ?>

            <div class="box">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h2 class="profile-username text-center"><strong><a href="#"><?= $antiXss->xss_clean($contract->name) ?></a></strong>
                        </h2>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 20%">Khoảng thu nhập</th>
                                <td><?= $contract->rank_salary ?></td>
                            </tr>

                            <tr>
                                <th>Khu vực muốn hợp tác</th>
                                <td><?= $antiXss->xss_clean($contract->address) ?></td>
                            </tr>

                            <tr>
                                <th>Họ và tên</th>
                                <td><?= $antiXss->xss_clean($contract->name) ?></td>
                            </tr>

                            <tr>
                                <th>Số điện thoại</th>
                                <td><?= $contract->phone ?></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><?= $contract->email ?></td>
                            </tr>

                            </tbody>
                        </table>
                        <div class="form-group col-sm-12 col-xs-12 row text-center">
                            <a href="/admin/Contract/index" class="btn btn-default">Thoát </a>
                        </div>
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

