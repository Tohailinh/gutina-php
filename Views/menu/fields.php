<div class="content">
    <?php include('../Views/layouts/error.php') ?>

    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <form method="POST" action="" class="form jsvalidation" name="admin_create" enctype="multipart/form-data">
                    <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                        <!-- Username Field -->
                        <div class="form-group col-sm-12">
                            <label for="name">Name :</label>
                            <span class="text-danger">(*)</span>
                            <input placeholder="Name" class="form-control" name="name" type="text"
                                   id="name" value="<?php echo $antiXss->xss_clean($menu->name) ?>" required >
                        </div>
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12 col-xs-12 row text-center">
                        <a href="/admin/Menu/index" class="btn btn-default">Thoát </a>

                        <input class="btn btn-success" type="submit" value="Lưu" name="submit">
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>

