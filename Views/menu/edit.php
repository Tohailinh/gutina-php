<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Cập nhật
        <small><?= $antiXss->xss_clean($menu->name) ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/Menu/index">Danh sách quản trị viên</a></li>
        <li><a href="/admin/Menu/edit?id=<?= $menu->id ?>">Cập nhật "<?= $antiXss->xss_clean($menu->name) ?>"</a></li>
    </ol>
</section>
<?php include '../Views/menu/fields.php' ?>