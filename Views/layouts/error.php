<?php
$errors_arr = $_SESSION['errors'];
if (count($errors_arr) > 0){
    ?>
    <ul class="alert alert-danger">
        <?php foreach ($errors_arr as $error): ?>
            <li><?php echo $error ?></li>
        <?php endforeach; ?>
    </ul>
<?php } ?>

<?php
if (!empty($_SESSION['errors'])){
    if (strlen(implode($_SESSION['errors'])) < 0) {
        $errors = $_SESSION['errors'];
        if (!empty($errors)) { ?>
            <ul class="alert alert-danger">
                <li><?php echo $errors ?></li>
            </ul>
            <?php unset($_SESSION['errors']);
        }
    }
    unset($_SESSION['errors']);
} ?>
