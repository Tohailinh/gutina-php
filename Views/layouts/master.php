<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Gutina | Admin</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <?php include '../Views/layouts/datatables_css.php' ?>
        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="../../index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">Gutina</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="/uploads/users/<?= $_SESSION['avatar'] ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $_SESSION['fullname'] ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="/uploads/users/<?= $_SESSION['avatar'] ?>" class="img-circle" alt="User Image">
                                    <p>
                                        <?= $_SESSION['fullname'] ?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="/admin/Login/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="">
                        <a href="/admin/Admin/index">
                            <i class="fa fa-user"></i> <span>Quản lý quản trị viên</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="/admin/Category/index">
                            <i class="fa fa-user"></i> <span>Quản lý danh mục</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="/admin/Contract/index">
                            <i class="fa fa-user"></i> <span>Quản lý đối tác </span>
                        </a>
                    </li>

                    <li class="">
                        <a href="/admin/Customers/index">
                            <i class="fa fa-user"></i> <span>Quản lý khách hàng </span>
                        </a>
                    </li>

                    <li class="">
                        <a href="/admin/News/index">
                            <i class="fa fa-user"></i> <span>Quản lý tin tức </span>
                        </a>
                    </li>

                    <li class="">
                        <a href="/admin/Menu/index">
                            <i class="fa fa-user"></i> <span>Quản lý menu </span>
                        </a>
                    </li>

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?= @$content ?>
        </div>
        <!-- /.content-wrapper -->
<!--        <footer class="main-footer">-->
<!--            <div class="pull-right hidden-xs">-->
<!--                <b>Version</b> 2.4.0-->
<!--            </div>-->
<!--            <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights-->
<!--            reserved.-->
<!--        </footer>-->
    </div>

    <?php include '../Views/layouts/datatables_js.php' ?>
    <script>
        // alert(document.cookie);
        $(function () {
            var t = $('#example1').DataTable({
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0,
                    'paging': false,
                    'lengthChange': false,
                    'searching': false,
                    'ordering': false,
                    'info': false,
                    'autoWidth': false
                }],
                "responsive": true,
                "order": [[1, 'asc']]
            });

            t.on('order.dt search.dt', function () {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

        // window.location.reload(true);
    </script>

    </body>
    </html>
