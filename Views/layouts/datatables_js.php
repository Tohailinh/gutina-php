<!-- /.login-box -->
<!-- jQuery 3 -->
<!--<script src="../../assets/bower_components/jquery/dist/jquery.min.js"></script>-->

<script src="../../../assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../../assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../../../assets/bower_components/datatables.net-bs/js/dataTables.buttons.js"></script>
<script src="../../../assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="../../../assets/bower_components/ckeditor/ckeditor.js"></script>
<script src="../../../assets/plugins/responsive/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<script src="../../../assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../../assets/plugins/moment/min/moment.min.js"></script>
<script src="../../../assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../../../assets/plugins/sweetalert/sweetalert.js"></script>
<script src="../../../assets/plugins/jquery-loading/src/loading.js"></script>

<script src="../../../assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../../assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../../assets/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../../assets/js/demo.js"></script>
<script src="../../../assets/js/myjs.js"></script>
<script src="../../../assets/js/tinymce/tinymce.min.js"></script>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();


    })
</script>

<script>
    $(document).ready(function() {
        tinymce.init({
            selector: "#content-selector",theme: "modern",height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true ,

            external_filemanager_path:"/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            // external_plugins: { "filemanager" : "../../../assets/filemanager/plugin.min.js"}
        });
    });
</script>