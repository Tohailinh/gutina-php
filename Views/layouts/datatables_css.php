
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="../../../assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="../../../assets/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="../../../assets/bower_components/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="../../../assets/bower_components/select2/dist/css/select2.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="../../../assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!--        <link rel="stylesheet" href="../../assets/bower_components/datatables.net/css/jquery.dataTables.min.css">-->

<link rel="stylesheet" href="../../../assets/plugins/responsive/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="../../../assets/plugins/sweetalert/sweetalert.css">
<link rel="stylesheet" href="../../../assets/plugins/jquery-loading/src/loading.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
<!--<script src="../../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"></script>-->

<!-- Theme style -->
<link rel="stylesheet" href="../../../assets/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../../assets/css/mycss.css">

<!-- iCheck -->
<link rel="stylesheet" href="../../../assets/plugins/iCheck/square/blue.css">
<link rel="stylesheet" href="../../../assets/css/skins/_all-skins.min.css">