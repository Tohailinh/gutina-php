<?php
$messages = $_SESSION['messages'][0];
if (!empty($messages)){ ?>
    <ul class="alert alert-success">
        <li><?php echo $messages ?></li>
    </ul>
<?php
    unset($_SESSION['messages']);
} ?>
