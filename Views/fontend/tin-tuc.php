<?php
use App\Models\Category;
use App\Models\News;
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tin tức – Gutina.vn</title>
    <meta name="description"
          content="Gutina - cập nhật liên tục tất cả các tin tức mới nhất về dịch vụ chuyển tiền nhanh 24/7, thông tin tài chính ngân hàng và các tin tổng hợp liên quan khác.">
    <meta charset="utf-8">
    <link rel="icon" href="/assets/fontend/images/icon.png" sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "../Views/fontend/css.php"; ?>
</head>
<body>
<div id="top">
    <div id="content" class="container">
        <div id="menu" class="row">
            <div class="menu_top">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/"><img class="logo_menu" src="/assets/fontend/images/logo_2_1.png"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="btn-cancel"><img src="/assets/fontend/images/simple-remove.png"></div>
                            <div class="clear"></div>
                            <ul class="nav navbar-nav">
                                <li class="item-menu"><a class="link-menu" href="/">Trang chủ</a></li>
                                <li class="dropdown item-menu ">
                                    <a href="#" class="dropdown-toggle link-menu " data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Chuyển tiền</a>
                                    <ul class="dropdown-menu">
                                        <a href="/rut-tien">
                                            <li><p>Rút tiền</p></li>
                                        </a>
                                        <a href="/nop-tien">
                                            <li><p>Nộp tiền</p></li>
                                        </a>
                                    </ul>
                                </li>
                                <li class="item-menu" id="btn-modal" data-toggle="modal" data-target="#myModal"><a class="link-menu" href="#">Đối tác</a></li>
                                <li class="item-menu "><a class="link-menu " href="/hoi-dap">Hỏi đáp</a></li>
                                <li class="dropdown item-menu active_menu">
                                    <a href="#" class="dropdown-toggle link-menu atv " data-toggle="dropdown"
                                       role="button"
                                       aria-haspopup="true" aria-expanded="false">Tin tức</a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($categories as $k => $category) {
                                            ?>
                                            <a href="/<?= $category->slug ?>">
                                                <li><p><?= $category->name ?></p></li>
                                            </a>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li class="item-menu"><a class="link-menu" href="/lien-he">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="clear"></div>
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h1>Tin tức</h1>
            </div>
            <div class="col-md-6 col-md-6 col-xs-6">
                <div class="bg-img bg-news">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-news">
    <div class="container">
        <div class="_content_news">
            <div class="row">
                <?php
                $limit = 9;
                $init = ($page-1)*$limit;
                $where = 1;
                $news = News::newCate($slug, $init, $limit);
                foreach ($news as $new) {
                    ?>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="row-news">
                            <div class="img-news">
                                <a href="/tin-tuc/<?= $new->slug ?>"><img src="/uploads/news/<?= $new->avatar ?>" alt="<?= $new->name ?>"></a>
                            </div>
                            <label><?= $new->created_at ?></label>
                            <h4><a href="/tin-tuc/<?= $new->slug ?>"><?= $new->name ?></a></h4>
                            <span class="desc"><a href="/tin-tuc/<?= $new->slug ?>">
                                   <?php
                                       $description = News::trimmedTitle($new->description);
                                       echo $description;
                                   ?>
                                </a>
                        </span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="load-more">
            <?php
                if($countItems>$limit){
                    $sotrang = ceil($countItems/$limit);
                    echo "<tr><td colspan='9' class='text-right'>";
                    News::pagination($slug, $page, $sotrang);
                    echo "</td></tr>";
                }

            ?>
        </div>
    </div>
</div>

<div class="line">
    <hr>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><img
                            src="/assets/fontend/images/simple-remove.png"></button>
                <h4 class="modal-title" id="myModalLabel">Trở thành đối tác của Gutina</h4>
                <label>Vui lòng điền thông tin đầy đủ và chính xác để Gutina có thể liên hệ sớm đến bạn sớm nhất</label>
                <form action="/doi-tac" method="post">
                    <select class="input-contract" name="rank_salary">
                        <option><p>Khoảng thu nhập: Trên 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 10 triệu - 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 5 triệu - 9 triệu</p></option>
                        <option><p>Khoảng thu nhập: 3 triệu - 5 triệu</p></option>
                        <option><p>Khoảng thu nhập: Dưới 3 triệu</p></option>
                    </select>
                    <span class="glyphicon glyphicon-menu-down icon-down" aria-hidden="true"></span>
                    <input type="text" name="address" class="input-contract location" required
                           placeholder="Khu vực muốn hợp tác">
                    <input type="text" name="name" class="input-contract full-name" required placeholder="Họ và tên">
                    <input type="text" name="phone" class="input-contract phone" required placeholder="Số điện thoại">
                    <input type="email" name="email" class="input-contract email" required placeholder="Email">
                    <button type="submit" class="btn-contract">Đăng ký ngay</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <a href="tel:+84962014588" class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle"></div>
    </a>
</div>
<?php include "../Views/fontend/footer.php"; ?>
</body>
<?php include "../Views/fontend/js.php"; ?>
</html>