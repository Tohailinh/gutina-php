<div class="footer">
    <div class="container">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="logo">
                <img src="/assets/fontend/images/logo_2.png">
                <h4><span class="txt-pc">Dịch vụ <a class="rm-decoration" href="http://g-pay.vn/chuyen-tien/">chuyển tiền nhanh</a></span><span
                        class="txt-mb"><a class="rm-decoration"
                                          href="http://g-pay.vn/chuyen-tien/">Chuyển tiền nhanh</a></span> theo mô hình
                    Uber<br> với đội ngũ nhân viên điều phối và giao dịch chuyên nghiệp, tận tâm.</h4>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_left">
                <div class="logo-1"><img src="/assets/fontend/images/pin-3.png"><span>Tầng M, Tòa B Hoàng Huy, 275 Nguyễn Trãi, Thanh Xuân, Hà Nội</span></div>
                <div class="logo-1"><img src="/assets/fontend/images/phone-2.png">
                    <span>
                        <a class="rm-decoration" href="tel:+84962014588">0962.014.588</a> |
                        <a class="rm-decoration" href="tel:+84947060588">0947.060.588</a>
                    </span>
                </div>
                <div class="logo-1"><img src="/assets/fontend/images/email-83.png"><span>gutina.247@gmail.com</span></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="map">
                <p class="txt-map">Chỉ đường trên google map</p>
                <div id="googleMap" style="width:100%;height:150px;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.986055165282!2d105.80538691540202!3d20.993196094366265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbf6af3c44f%3A0xed2a3283251a5bf7!2zMjc1IE5ndXnhu4VuIFRyw6NpLCBUaGFuaCBYdcOibiBUcnVuZywgVGhhbmggWHXDom4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1546402222762"
                            frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_right">
                <form method="post" action="https://gutina.vn/email">
                    <h4>Đăng ký nhận ưu đãi</h4>
                    <div class="btn-email">
                        <input type="email" name="email" required placeholder="Email của bạn">
                        <button type="submit" class="btn btn-success">Gửi</button>
                    </div>
                </form>
                <a href="https://www.facebook.com/Gutina247/"><img src="/assets/fontend/images/Facebook_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Google_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Youtube_Icon.png"></a>
            </div>
        </div>
    </div>
</div>