<!DOCTYPE html>
<html>
<head>
    <title>Nộp tiền – Gutina.vn</title>
    <meta name="description" content="Nộp tiền Gutina giúp bạn giải quyết tất cả các vấn đề về: nộp tiền vào tài khoản, nộp tiền ngoài giờ, nộp tiền tài khoản chủ nhật, nộp tiền ngày lễ,...">
    <meta charset="utf-8">
    <link rel="icon" href="/assets/fontend/images/icon.png" sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../assets/fontend/css/transfer.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/fontend/css/download-app.min.css">
    <?php include "../Views/fontend/css.php"; ?>
    <!-- Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->
<div id="top">
    <div id="content" class="container">
        <div id="menu" class="row">
            <div class="menu_top">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/"><img class="logo_menu" src="/assets/fontend/images/logo_2_1.png"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="btn-cancel"><img src="/assets/fontend/images/simple-remove.png"></div>
                            <div class="clear"></div>
                            <ul class="nav navbar-nav">
                                <li class="item-menu"><a class="link-menu" href="/">Trang chủ</a></li>
                                <li class="dropdown item-menu active_menu">
                                    <a href="#" class="dropdown-toggle link-menu atv" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Chuyển tiền</a>
                                    <ul class="dropdown-menu">
                                        <a href="/rut-tien">
                                            <li><p>Rút tiền</p></li>
                                        </a>
                                        <a href="/nop-tien">
                                            <li><p>Nộp tiền</p></li>
                                        </a>
                                    </ul>
                                </li>
                                <li class="item-menu" id="btn-modal" data-toggle="modal" data-target="#myModal"><a
                                        class="link-menu" href="#">Đối tác</a></li>
                                <li class="item-menu "><a class="link-menu " href="/hoi-dap">Hỏi đáp</a></li>
                                <li class="dropdown item-menu ">
                                    <a href="#" class="dropdown-toggle link-menu " data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Tin tức</a>
                                    <ul class="dropdown-menu">
                                        <a href="/tin-tuc">
                                            <li><p>Tin tức tổng hợp</p></li>
                                        </a>
                                        <a href="tu-van-chuyen-tien.html">
                                            <li><p>Tư vấn chuyển tiền</p></li>
                                        </a>
                                        <a href="tai-chinh-ngan-hang.html">
                                            <li><p>Tài chính - Ngân hàng</p></li>
                                        </a>
                                        <a href="khuyen-mai.html">
                                            <li><p>Khuyến mãi</p></li>
                                        </a>

                                    </ul>
                                </li>
                                <li class="item-menu"><a class="link-menu " href="/lien-he">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="bg-transfer">
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="bg-transfer-1">
                <h1 class="title-transfer">Nộp tiền nhanh <br> toàn quốc 24/7</h1>
                <div class="tt">
                    <div class="title-1">
                        <h4>Không giao dịch được<b> vì đã quá giờ hành chính?</b></h4>
                    </div>
                    <div class="title-1">
                        <h4>Không giao dịch được <b>vì Thứ 7, CN ngân hàng không làm việc?</b></h4>
                    </div>
                    <div class="title-1">
                        <h4>Ám ảnh cảnh chen chúc <b>đợi lấy số ở ngân hàng?</b></h4>
                    </div>
                    <div class="title-1">
                        <h4>Ngại đến ngân hàng vì <b>thời tiết nắng nóng, mưa bão?</b></h4>
                    </div>
                    <div class="title-1">
                        <h4>Mòn mỏi chờ tiền nổi vì <b>trót chuyển tiền ngoài hệ thống?</b></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <a href="tel:+84962014588">
                <div class="contact">
                    <img src="/assets/fontend/images/phone-3.png">
                    <span>Gọi ngay <b>0962 014 588</b></span>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="model">
    <div class="container">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <img class="beauti lazyload" src="/assets/fontend/images/beauti1.png">
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <h4 class="title-to-do">Với Gutina, bạn có thể</h4>

            <div class="to-do-1">
                <img class="lazyload" data-original="/assets/fontend/images/mobile-card.png" data-src="mobile-card.png">
                <span>Gửi/rút tiền, nộp tiền, chuyển khoản vào <br> <b>tài khoản ngân hàng</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" data-original="/assets/fontend/images/distance.png" data-src="distance.png">
                <span>Khoảng cách không còn là vấn đề<br> <b>với phí dịch vụ chỉ từ 30.000đ</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" data-original="/assets/fontend/images/handout.png" data-src="handout.png">
                <span>Giao dịch tại nhà với mức phí thấp nhất,<br> <b> giảm tới 40% trong giờ hành chính</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" data-original="/assets/fontend/images/bank.png" data-src="bank.png">
                <span>Thực hiện giao dịch<br> <b>trên 40 ngân hàng</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" data-original="/assets/fontend/images/sun-cloud-rain.png" data-src="sun-cloud-rain.png">
                <span>Tạm biệt hàng giờ đồng hồ<br> <b>tắc đường, nắng mưa</b></span>
            </div>
        </div>
    </div>
</div>

<div class="rank_work_2">
    <div class="container">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="rw-title">
                <h4 class="rw-title-1">Khung giờ</h4>
                <h4 class="rw-title-1">làm việc</h4>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="rw-right">

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="item-hour">
                        <img class="lazyload" src="/assets/fontend/images/time-clock.png" data-src="time-clock.png">
                        <div class="aa">
                            <span class="rw-title-1">24&nbsp;</span><p> giờ/ngày</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="item-hour">
                        <img class="lazyload" src="/assets/fontend/images/calendar-grid-58.png" data-src="calendar-grid-58.png">
                        <div class="aa">
                            <span class="rw-title-1">7&nbsp;</span><p> ngày/tuần</p>
                        </div>
                    </div>
                </div>

            </div>
            <p class="rw-footer">Tất cả các ngày nghỉ lễ - Bất kể giờ giấc thời tiết</p>
        </div>

    </div>
</div>
<div class="rank_work">
    <div class="rw-left">
    </div>
    <div class="rw-left-1"></div>
</div>

<div class="procedure">
    <div class="title--step">
        <h4>Quy trình giao dịch</h4>
    </div>
    <div class="wrapper">
        <div class="timeline">
            <dl class="timeline--entry">
                <dt class="timeline--entry__title">
                    <img class="img-step img--1 lazyload" data-original="/assets/fontend/images/icon_1.png" data-src="icon_1.png">
                    <span class="title-step tt-1">Liên hệ với Gutina thông qua <br>Hotline: <a class="rm-decoration" href="tel:+84962014588">0962.014.588</a> - <a class="rm-decoration" href="tel:+84947060588">0947.060.588</a> <br>hoặc fanpage:
                        <br> <a class="rm-decoration" href="https://www.facebook.com/Gutina247/">“Gutina - Chuyển tiền nhanh toàn quốc 24/7”</a></span>
                </dt>
            </dl>
            <dl class="timeline--entry">
                <dt class="timeline--entry__title">
                    <img class="img-step lazyload" data-original="/assets/fontend/images/icon_2.png" data-src="icon_2.png">
                    <span class="title-step tt-2">Lựa chọn hình thức giao dịch tận nhà hoặc giao dịch tại Văn phòng Gutina, nhận thông báo về Phí dịch vụ, Phí ship (nếu có), và xác nhận yêu cầu.</span>
                </dt>
            </dl>
            <dl class="timeline--entry">
                <dt class="timeline--entry__title">
                    <img class="img-step lazyload" data-original="/assets/fontend/images/icon_3.png" data-src="icon_3.png">
                    <span class="title-step tt-3">
                        Nhân viên giao dịch của Gutina đến địa chỉ được yêu cầu, nhận tiền, viết biên lai và đóng dấu đỏ.
                    </span>
                </dt>
            </dl>
            <dl class="timeline--entry">
                <dt class="timeline--entry__title">
                    <img class="img-step lazyload" data-original="/assets/fontend/images/icon_4.png" data-src="icon_4.png">
                    <span class="title-step tt-4">
                        Bộ phận kỹ thuật xác nhận thông tin và thực hiện thao tác
                    </span>
                </dt>
            </dl>
            <dl class="timeline--entry">
                <dt class="timeline--entry__title">
                    <img class="img-step lazyload" data-original="/assets/fontend/images/icon_5.png" data-src="icon_5.png">
                    <span class="title-step tt-5">
                        Giao dịch hoàn thành chỉ trong 5 phút
                    </span>
                </dt>
            </dl>
            <dl class="timeline--entry">
                <dt class="timeline--entry__title">
                    <img class="img-step lazyload" data-original="/assets/fontend/images/icon_6.png" data-src="icon_6.png">
                    <span class="title-step tt-6">
                        Nhân viên giao dịch gửi khách hàng biên lai, xác nhận thông tin Giao dịch thành công.
                    </span>
                </dt>
            </dl>

        </div>
    </div>
    <div class="container abc">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <a href="tel:+84962014588">
                <div class="btn-price btn-left">
                    <p>Chuyển tiền ngay</p>
                    <span>0962 014 588</span>
                </div>
            </a>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <a href="tel:+84947060588">
                <div class="btn-price btn-right">
                    <p>Báo giá ưu đãi KH doanh nghiệp</p>
                    <span>0947 060 588</span>
                </div>
            </a>
        </div>
    </div>
</div>
<div id="down-app">
    <div class="container">
        <div class="row bg-1">
            <div class="col-md-5 col-sm-12 col-xs-12">
                <h4>Tải ứng dụng Gutina</h4>
                <div class="img-download">
                    <a href="itms-services://?action=download-manifest&amp;url=https://gutina.vn/app.plist">
                        <img class="lazyload" data-src="app-store.png" data-original="/assets/fontend/images/app-store.png">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=vn.gutina.origin&hl=vi">
                        <img class="lazyload" data-src="google-play.png" data-original="/assets/fontend/images/google-play.png">
                    </a>
                </div>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
                <img class="img-example lazyload" data-src="/assets/fontend/images/image-app.png" data-original="/assets/fontend/images/image-app.png">
            </div>
        </div>
    </div>
</div><div id="content_4">
    <div class="container">
        <h4 class="title">Khách hàng</h4>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 text-center">

                <img class="prev_carousel_2" src="/assets/fontend/images/left_1.png">
                <div id="owl-carousel-2" class="owl-carousel owl-theme text-left">
                    <div class="slide_1">
                        <div class="avatar">
                            <img class="lazyload" data-original="/assets/fontend/images/15-44-54-2018-10-18-avatar-1.png" data-src="15-44-54-2018-10-18-avatar-1.png">
                            <div class="name">
                                <h3>Chị Thu Hằng</h3>
                                <h4>Kế toán</h4>
                            </div>
                        </div>
                        <div class="content">
                            <div class="rating">
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  star "></span>
                            </div>
                            <h4>Không còn phải khổ sở tìm chỗ đỗ xe khi phải đi ngân hàng giao dịch</h4>
                            <label class="txt-pc">Điều tôi ghét nhất khi ra ngân hàng giao dịch là đa số ngân hàng không có chỗ đỗ oto hẳn hoi. Tôi đánh liều đỗ xe 3 lần thì cả 3 lần bị công an hốt đi. Tôi đã ước có dịch vụ nào như Gutina thế này lâu lắm rồi, nhanh lại tiện, khỏi phải đến ngân hàng rồi đến khổ với tìm chỗ đỗ xe. Cảm ơn Gutina!</label>
                        </div>
                    </div>
                    <div class="slide_1">
                        <div class="avatar">
                            <img class="lazyload" data-original="/assets/fontend/images/15-45-44-2018-10-18-avatar-2.png" data-src="15-45-44-2018-10-18-avatar-2.png">
                            <div class="name">
                                <h3>Bạn Hoàng Hà</h3>
                                <h4>Sinh viên</h4>
                            </div>
                        </div>
                        <div class="content">
                            <div class="rating">
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                            </div>
                            <h4>Gutina thực sự thuận tiền khi tôi cần chuyển, rút tiền</h4>
                            <label class="txt-pc">Tôi thực sự hài lòng về trải nghiệm sản phẩm mà công ty mang lại. Chất lượng sản phẩm hoàn hảo, dịch vụ chăm sóc khách hàng tận tâm. Giá cả sản phẩm hợp lý. Tôi sẽ ủng hộ công ty thường xuyên vào thời gian tới.</label>
                        </div>
                    </div>
                    <div class="slide_1">
                        <div class="avatar">
                            <img class="lazyload" data-original="/assets/fontend/images/15-46-31-2018-10-18-avatar-3.png" data-src="15-46-31-2018-10-18-avatar-3.png">
                            <div class="name">
                                <h3>Anh Dương Hoàng</h3>
                                <h4>Nhân viên văn phòng</h4>
                            </div>
                        </div>
                        <div class="content">
                            <div class="rating">
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                            </div>
                            <h4>Tôi không còn lo hàng giờ phải chen chúc ở ngân hàng</h4>
                            <label class="txt-pc">Tôi thực sự hài lòng về trải nghiệm sản phẩm mà công ty mang lại. Chất lượng sản phẩm hoàn hảo, dịch vụ chăm sóc khách hàng tận tâm. Giá cả sản phẩm hợp lý. Tôi sẽ ủng hộ công ty thường xuyên vào thời gian tới.</label>
                        </div>
                    </div>

                </div>
                <img class="next_carousel_2" src="/assets/fontend/images/right_1.png">

            </div>


        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><img src="/assets/fontend/images/simple-remove.png"></button>
                <h4 class="modal-title" id="myModalLabel">Trở thành đối tác của Gutina</h4>
                <label>Vui lòng điền thông tin đầy đủ và chính xác để Gutina có thể liên hệ sớm đến bạn sớm nhất</label>
                <form action="/doi-tac" method="post">
                    <select class="input-contract" name="rank_salary">
                        <option><p>Khoảng thu nhập: Trên 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 10 triệu - 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 5 triệu - 9 triệu</p></option>
                        <option><p>Khoảng thu nhập: 3 triệu - 5 triệu</p></option>
                        <option><p>Khoảng thu nhập: Dưới 3 triệu</p></option>
                    </select>
                    <span class="glyphicon glyphicon-menu-down icon-down" aria-hidden="true"></span>
                    <input type="text" name="address" class="input-contract location" required placeholder="Khu vực muốn hợp tác">
                    <input type="text" name="name" class="input-contract full-name" required placeholder="Họ và tên">
                    <input type="text" name="phone" class="input-contract phone" required placeholder="Số điện thoại">
                    <input type="email" name="email" class="input-contract email" required placeholder="Email">
                    <button type="submit" class="btn-contract">Đăng ký ngay</button>
                </form>
            </div>
        </div>
    </div>
</div><div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <a href="tel:+84962014588" class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle"></div>
    </a>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="logo">
                <img src="/assets/fontend/images/logo_2.png">
                <h4><span class="txt-pc">Dịch vụ <a class="rm-decoration" href="http://g-pay.vn/chuyen-tien/">chuyển tiền nhanh</a></span><span class="txt-mb"><a class="rm-decoration" href="http://g-pay.vn/chuyen-tien/">Chuyển tiền nhanh</a></span> theo mô hình Uber<br> với đội ngũ nhân viên điều phối và giao dịch chuyên nghiệp, tận tâm.</h4>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_left">
                <div class="logo-1"><img src="/assets/fontend/images/pin-3.png"><span>Tầng M, Tòa B Hoàng Huy, 275 Nguyễn Trãi, Thanh Xuân, Hà Nội</span></div>
                <div class="logo-1"><img src="/assets/fontend/images/phone-2.png"><span><a class="rm-decoration" href="tel:+84962014588">0962.014.588</a> | <a class="rm-decoration" href="tel:+84947060588">0947.060.588</a></span></div>
                <div class="logo-1"><img src="/assets/fontend/images/email-83.png"><span>gutina.247@gmail.com</span></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="map">
                <p class="txt-map">Chỉ đường trên google map</p>
                <div id="googleMap" style="width:100%;height:150px;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.986055165282!2d105.80538691540202!3d20.993196094366265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbf6af3c44f%3A0xed2a3283251a5bf7!2zMjc1IE5ndXnhu4VuIFRyw6NpLCBUaGFuaCBYdcOibiBUcnVuZywgVGhhbmggWHXDom4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1546402222762"
                            frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_right">
                <form method="post" action="/email">
                    <input type="hidden" name="_token" value="iwdebujj1wCsazXa53djunLzfW5Ue5FCfhhz8t6Y">
                    <h4>Đăng ký nhận ưu đãi</h4>
                    <div class="btn-email">
                        <input type="email" name="email" required placeholder="Email của bạn">
                        <button type="submit" class="btn btn-success">Gửi</button>
                    </div>
                </form>
                <a href="https://www.facebook.com/Gutina247/"><img src="/assets/fontend/images/Facebook_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Google_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Youtube_Icon.png"></a>
            </div>
        </div>
    </div>
</div>
</body>

<?php include "../Views/fontend/js.php"; ?>
</html>