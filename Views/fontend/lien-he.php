<?php
use App\Models\Category;
$where = 1;
$where .= " AND parent_id = 0";
$categories = Category::readAll('category', $where);
?>
<!DOCTYPE html>
<html>
<head>
    <title> Liên hệ Gutina</title>
    <meta name="description"
          content="Gutina - cập nhật liên tục tất cả các tin tức mới nhất về dịch vụ chuyển tiền nhanh 24/7, thông tin tài chính ngân hàng và các tin tổng hợp liên quan khác.">
    <meta charset="utf-8">
    <link rel="icon" href="../../../assets/fontend//assets/fontend/images/icon.png" sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "../Views/fontend/css.php"; ?>

</head>
<body>
<!-- End Google Tag Manager (noscript) -->
<div id="top">
    <div id="content" class="container">
        <div id="menu" class="row">
            <div class="menu_top">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/"><img class="logo_menu" src="/assets/fontend/images/logo_2_1.png"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="btn-cancel"><img src="/assets/fontend/images/simple-remove.png"></div>
                            <div class="clear"></div>
                            <ul class="nav navbar-nav">
                                <li class="item-menu"><a class="link-menu" href="/">Trang chủ</a></li>
                                <li class="dropdown item-menu ">
                                    <a href="#" class="dropdown-toggle link-menu " data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Chuyển tiền</a>
                                    <ul class="dropdown-menu">
                                        <a href="/rut-tien">
                                            <li><p>Rút tiền</p></li>
                                        </a>
                                        <a href="/nop-tien">
                                            <li><p>Nộp tiền</p></li>
                                        </a>
                                    </ul>
                                </li>
                                <li class="item-menu" id="btn-modal" data-toggle="modal" data-target="#myModal"><a
                                        class="link-menu" href="#">Đối tác</a></li>
                                <li class="item-menu "><a class="link-menu " href="/hoi-dap">Hỏi đáp</a></li>
                                <li class="dropdown item-menu ">
                                    <a href="#" class="dropdown-toggle link-menu " data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Tin tức</a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($categories as $k => $category){
                                            ?>
                                            <a href="/<?= $category->slug ?>">
                                                <li><p><?= $category->name ?></p></li>
                                            </a>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li class="item-menu  active_menu ">
                                    <a class="link-menu atv " href="/lien-he">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h1>Liên hệ</h1>
            </div>
            <div class="col-md-6 col-md-6 col-xs-6">
                <div class="bg-img bg-img-contact">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6">
                <div class="contact-1">
                    <img src="/assets/fontend/images/single-02.png">
                    <h4>Khách hàng cá nhân</h4>
                    <p>0962.014.588 - 0947.060.588</p>
                    <a href="tel:+84962014588">
                        <button>Liên hệ ngay</button>
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-xs-12 col-sm-6">
                <div class="contact-1">
                    <img src="/assets/fontend/images/briefcase-24.png">
                    <h4>Khách hàng doanh nghiệp</h4>
                    <p>0918.499.822</p>
                    <a href="tel:+84918499822">
                        <button>Liên hệ ngay</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="contact-2">
                    <div class="contact-2-0">
                        <img src="/assets/fontend/images/email-83-2.png">
                        <div class="contact-2-1">
                            <p class="lb-name">Email:</p>
                            <p>gutina.247@gmail.com</p>
                        </div>

                    </div>

                    <div class="contact-2-0">
                        <img src="/assets/fontend/images/facebook.png">
                        <div class="contact-2-1">
                            <p class="lb-name">Facebook:</p>
                            <p><a href="#">https://www.facebook.com/<br>Gutina247/</a></p>
                        </div>
                    </div>

                    <div class="contact-2-0">
                        <img src="/assets/fontend/images/webpage.png">
                        <div class="contact-2-1">
                            <p class="lb-name">Website:</p>
                            <p><a href="#"> https://gutina.vn/</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.986055165282!2d105.80538691540202!3d20.993196094366265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbf6af3c44f%3A0xed2a3283251a5bf7!2zMjc1IE5ndXnhu4VuIFRyw6NpLCBUaGFuaCBYdcOibiBUcnVuZywgVGhhbmggWHXDom4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1546402222762"
            frameborder="0" style="border:0" width="100%" height="450px" allowfullscreen></iframe>
</div>
<?php include "../Views/fontend/modal.php"; ?>
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <a href="tel:+84962014588" class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle"></div>
    </a>
</div>
<?php include "../Views/fontend/footer.php"; ?>
</body>

<?php include "../Views/fontend/js.php"; ?>
</html>