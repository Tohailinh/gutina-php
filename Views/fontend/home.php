<!DOCTYPE html>
<html>
<head>
    <title> Gutina - Chuyển tiền nhanh toàn quốc 24/7 </title>
    <meta name="description"
          content="Gutina - Chuyển tiền nhanh toàn quốc 24/7. Cung cấp các dịch vụ liên quan đến hoạt động chuyển/rút/gửi tiền vào ngân hàng trong và ngoài giờ hành chính.">
    <meta charset="utf-8">
    <link rel="icon" href="../../../assets/fontend/assets/fontend/images/icon.png" sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "../Views/fontend/css.php"; ?>
    <link rel="stylesheet" type="text/css" href="../../../assets/fontend/css/index.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/fontend/css/menu.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/fontend/css/download-app.min.css">
    <link rel="stylesheet" type="text/css" href="../../../assets/fontend/css/register.min.css">
</head>

<body>
<!-- End Google Tag Manager (noscript) -->
<div id="top">
    <div id="content" class="container">
        <?php include "../Views/fontend/menu.php"; ?>
    </div>
</div>
<div id="banner">
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 sologan">
            <h1>Chuyển tiền nhanh <br class="txt-pc"> toàn quốc 24/7</h1>
            <div class="text_1">
                <h4><i class="fa fa-caret-right"></i>Ám ảnh cảnh chen chúc đợi lấy số ở ngân hàng?</h4>
                <h4><i class="fa fa-caret-right"></i>Không giao dịch được vì đã quá giờ hành chính</h4>
                <h4><i class="fa fa-caret-right"></i>Ngại đến ngân hàng vì thời tiết nắng nóng, mưa bão?</h4>
                <h4><i class="fa fa-caret-right"></i>Không giao dịch được vì Thứ 7, CN ngân hàng không làm việc?</h4>
                <h4><i class="fa fa-caret-right"></i>Mòn mỏi chờ tiền nổi vì trót chuyển tiền ngoài hệ thống?</h4>
            </div>


        </div>
    </div>
</div>
<div class="model">
    <div class="container">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div>
                <img class="beauti lazyload" src="/assets/fontend/images/beauti1.png" data-src="beauti1.png">
            </div>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <h4 class="title-to-do">Với Gutina, bạn có thể</h4>

            <div class="to-do-1">
                <img class="lazyload" src="/assets/fontend/images/mobile-card.png" data-src="mobile-card.png">
                <span>Gửi/rút tiền, nộp tiền, chuyển khoản<br> <b>vào tài khoản ngân hàng</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" src="/assets/fontend/images/distance.png" data-src="distance.png">
                <span>Khoảng cách không còn là vấn đề<br> <b>với phí dịch vụ chỉ từ 30.000đ</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" src="/assets/fontend/images/handout.png" data-src="handout.png">
                <span>Giao dịch tại nhà với mức phí thấp nhất,<br> <b> giảm tới 40% trong giờ hành chính</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" src="/assets/fontend/images/bank.png" data-src="bank.png">
                <span>Thực hiện giao dịch<br> <b>trên 40 ngân hàng</b></span>
            </div>

            <div class="to-do-1">
                <img class="lazyload" src="/assets/fontend/images/sun-cloud-rain.png" data-src="sun-cloud-rain.png">
                <span>Tạm biệt hàng giờ đồng hồ<br> <b>tắc đường, nắng mưa</b></span>
            </div>
        </div>
    </div>
</div>
<div id="content_1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="introduce">
                    <img src="/assets/fontend/images/img-1.png">
                    <div class="introduce_1">
                        <h4>Chuyển tiền nhanh <br> toàn quốc 24/7</h4>
                        <label>
                            Bất cứ lúc nào bạn muốn, ngay cả ngoài giờ hành chính, thứ 7, CN, ngày lễ tết
                        </label>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="introduce">
                    <img src="/assets/fontend/images/img-2.png">
                    <div class="introduce_1">
                        <h4>Lần đầu tiên áp dụng <br>công nghệ thông tin</h4>
                        <label>
                            Trong lĩnh vực tài chính, tất cả giao dịch được thực hiện nhanh chóng, thuận tiện
                        </label>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="introduce">
                    <img src="/assets/fontend/images/img-3.png">
                    <div class="introduce_1">
                        <h4>Giao dịch tận nơi</h4>
                        <label>
                            Với đội ngũ nhân viên điều phối và giao dịch chuyên nghiệp, tận tâm
                        </label>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="introduce">
                    <img src="/assets/fontend/images/img-4.png">
                    <div class="introduce_1">
                        <h4>Tiết kiệm thời gian,<br> chi phí</h4>
                        <label>
                            Tiền nổi ngay sau 7s, hoàn thành giao dịch 1 cách nhanh nhất có thể
                        </label>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="clear"></div>
<div id="content_2">
    <div class="container">
        <h4 class="title">Chỉ với 3 bước đơn giản</h4>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 set-bottom">
                <div class="step">
                    <div class="step-1">
                        <img class="step-img-1" src="/assets/fontend/images/lien_he.png">
                        <img class="step-img-2" src="/assets/fontend/images/right.png">
                    </div>
                    <div class="step-2">
                        <h3>1</h3>
                        <div class="step-2-1">
                            <h4>Liên hệ Guitina</h4>
                            <span class="img-1"><img class="verify" src="/assets/fontend/images/down.png"><a class="rm-decoration"
                                                                                             href="tel:+84962014588">0962.014.588</a></span>
                            <span class="img-2"><img class="verify set-height" src="/assets/fontend/images/phone.png"></span>
                            <span class="img-3"><img class="verify" src="/assets/fontend/images/up.png"><a class="rm-decoration"
                                                                                           href="tel:+84947060588">0947.060.588</a></span>
                            <span><img class="verify set-height" src="/assets/fontend/images/facebook.png"><a
                                    href=" https://www.facebook.com/Gutina247/">Facebook</a></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 set-bottom">
                <div class="step">
                    <div class="step-1">
                        <img class="step-img-1" src="/assets/fontend/images/xac_nhan.png">
                        <img class="step-img-2" src="/assets/fontend/images/right.png">
                    </div>
                    <div class="step-2">
                        <h3>2</h3>
                        <div class="step-2-1">
                            <h4>Xác nhận</h4>
                            <span><img class="verify" src="/assets/fontend/images/verify.png">Hình thức giao dịch</span>
                            <span><img class="verify" src="/assets/fontend/images/verify.png">Phí dịch vụ</span>
                            <span><img class="verify" src="/assets/fontend/images/verify.png">Phí ship</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 set-bottom">
                <div class="step">
                    <div class="step-1">
                        <img class="step-img-1" src="/assets/fontend/images/thuc_hien.png">
                    </div>
                    <div class="step-2">
                        <h3>3</h3>
                        <div class="step-2-1">
                            <h4>Thực hiện</h4>
                            <span><img class="verify" src="/assets/fontend/images/verify.png">Nhân viên nhận tiền</span>
                            <span><img class="verify" src="/assets/fontend/images/verify.png">Viết biên lai đóng dấu</span>
                            <span><img class="verify" src="/assets/fontend/images/verify.png">Tiền nổi sau 7s</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="register">
    <div class="register">
        <div class="register-form">
            <form>
                <label class="font-montserrat-bold">Đăng ký nhận tư vấn về dịch vụ - <span
                        class="red">miễn phí</span></label>
                <input class="input-register" type="text" id="r_name" name="r_name" placeholder="Họ và tên" required>
                <input class="input-register" type="text" id="r_phone_number" name="r_phone_number"
                       placeholder="Số điện thoại" required>
                <input class="input-register btn-register font-montserrat-bold" id="btn_register" type="button"
                       value="NHẬN TƯ VẤN MIỄN PHÍ">
            </form>
        </div>
    </div>
</div>
<div id="down-app">
    <div class="container">
        <div class="row bg-1">
            <div class="col-md-5 col-sm-12 col-xs-12">
                <h4>Tải ứng dụng Gutina</h4>
                <div class="img-download">
                    <a href="itms-services://?action=download-manifest&amp;url=https://gutina.vn/app.plist">
                        <img class="lazyload" data-src="app-store.png" src="/assets/fontend/images/app-store.png">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=vn.gutina.origin&hl=vi">
                        <img class="lazyload" data-src="google-play.png" src="/assets/fontend/images/google-play.png">
                    </a>
                </div>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
                <img class="img-example lazyload" data-src="image-app.png" src="/assets/fontend/images/image-app.png">
            </div>
        </div>
    </div>
</div>
<div id="content_3">
    <div class="container">
        <h4 class="title">Đối tác</h4>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <img class="prev_carousel_1" src="/assets/fontend/images/left_1.png">

                <div id="owl-carousel-1" class="owl-carousel owl-theme">
                    <div class="slide"><img src="/assets/fontend/images/tima.png?v=1"></div>
                    <div class="slide"><img src="/assets/fontend/images/f88.png?v=1"></div>
                    <div class="slide"><img src="/assets/fontend/images/gpay.png?v=1"></div>
                    <div class="slide"><img src="/assets/fontend/images/beat.png?v=1"></div>
                    <div class="slide"><img src="/assets/fontend/images/coffehouse.png?v=1"></div>
                    <div class="slide"><img src="/assets/fontend/images/me_cash.png?v=1"></div>
                </div>
                <img class="next_carousel_1" src="/assets/fontend/images/right_1.png">

            </div>

        </div>
    </div>
</div>
<div id="content_4">
    <div class="container">
        <h4 class="title">Khách hàng</h4>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 text-center">

                <img class="prev_carousel_2" src="/assets/fontend/images/left_1.png">
                <div id="owl-carousel-2" class="owl-carousel owl-theme text-left">
                    <div class="slide_1">
                        <div class="avatar">
                            <img src="/assets/fontend/images/15-44-54-2018-10-18-avatar-1.png">
                            <div class="name">
                                <h3>Chị Thu Hằng</h3>
                                <h4>Kế toán</h4>
                            </div>
                        </div>
                        <div class="content">
                            <div class="rating">
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  star "></span>
                            </div>
                            <h4>Không còn phải khổ sở tìm chỗ đỗ xe khi phải đi ngân hàng giao dịch</h4>
                            <label class="txt-pc">Điều tôi ghét nhất khi ra ngân hàng giao dịch là đa số ngân hàng không
                                có chỗ đỗ oto hẳn hoi. Tôi đánh liều đỗ xe 3 lần thì cả 3 lần bị công an hốt đi. Tôi đã
                                ước có dịch vụ nào như Gutina thế này lâu lắm rồi, nhanh lại tiện, khỏi phải đến ngân
                                hàng rồi đến khổ với tìm chỗ đỗ xe. Cảm ơn Gutina!</label>
                        </div>
                    </div>
                    <div class="slide_1">
                        <div class="avatar">
                            <img src="/assets/fontend/images/15-45-44-2018-10-18-avatar-2.png">
                            <div class="name">
                                <h3>Bạn Hoàng Hà</h3>
                                <h4>Sinh viên</h4>
                            </div>
                        </div>
                        <div class="content">
                            <div class="rating">
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                            </div>
                            <h4>Gutina thực sự thuận tiền khi tôi cần chuyển, rút tiền</h4>
                            <label class="txt-pc">Tôi thực sự hài lòng về trải nghiệm sản phẩm mà công ty mang lại. Chất
                                lượng sản phẩm hoàn hảo, dịch vụ chăm sóc khách hàng tận tâm. Giá cả sản phẩm hợp lý.
                                Tôi sẽ ủng hộ công ty thường xuyên vào thời gian tới.</label>
                        </div>
                    </div>
                    <div class="slide_1">
                        <div class="avatar">
                            <img src="/assets/fontend/images/15-46-31-2018-10-18-avatar-3.png">
                            <div class="name">
                                <h3>Anh Dương Hoàng</h3>
                                <h4>Nhân viên văn phòng</h4>
                            </div>
                        </div>
                        <div class="content">
                            <div class="rating">
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                                <span class="fa fa-star  checked "></span>
                            </div>
                            <h4>Tôi không còn lo hàng giờ phải chen chúc ở ngân hàng</h4>
                            <label class="txt-pc">Tôi thực sự hài lòng về trải nghiệm sản phẩm mà công ty mang lại. Chất
                                lượng sản phẩm hoàn hảo, dịch vụ chăm sóc khách hàng tận tâm. Giá cả sản phẩm hợp lý.
                                Tôi sẽ ủng hộ công ty thường xuyên vào thời gian tới.</label>
                        </div>
                    </div>

                </div>
                <img class="next_carousel_2" src="/assets/fontend/images/right_1.png">

            </div>


        </div>
    </div>
</div>
<?php include "../Views/fontend/modal.php"; ?>
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <a href="tel:+84962014588" class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle"></div>
    </a>
</div>
<?php include "../Views/fontend/footer.php"; ?>
</body>
<?php include "../Views/fontend/js.php"; ?>

<!-- Load Facebook SDK for JavaScript -->
<script>
    $(document).ready(function () {
        $('#btn_register').click(function () {
            var name = $('#r_name').val();
            var phone = $('#r_phone_number').val();

            if (name === '' || phone === '') {
                alert('Vui lòng nhập đầy đủ tên và số điện thoại');
            }
            var base_url = window.location.origin;

            $.ajax({
                url: base_url + '/dang-ky-tu-van?r_name=' + name + '&r_phone_number=' + phone,
                success: function (data) {
                    if (data == 1) {
                        alert('Đăng ký nhận tư vấn thành công');
                        $('#r_name').val('');
                        $('#r_phone_number').val('');
                    } else {
                        alert('Có lỗi xảy ra, vui lòng đăng ký lại');
                    }
                }
            });

        });
    });
</script>
</html>