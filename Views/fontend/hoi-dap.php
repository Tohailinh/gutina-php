<?php
use App\Models\Category;
$where = 1;
$where .= " AND parent_id = 0";
$categories = Category::readAll('category', $where);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Hỏi đáp Gutina</title>
    <meta name="description"
          content="Gutina chia sẻ và giải đáp tất cả các thắc mắc của khách hàng về vấn đề liên quan đến dịch vụ chuyển tiền nhanh. Đội ngũ nhân viên chuyên nghiệp và tận tâm.">
    <meta charset="utf-8">
    <link rel="icon" href="/assets/fontend/images/icon.png" sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "../Views/fontend/css.php"; ?>

    <!-- Google Tag Manager -->
</head>
<body>
<!-- End Google Tag Manager (noscript) -->
<div id="top">
    <div id="content" class="container">
        <div id="menu" class="row">
            <div class="menu_top">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/"><img class="logo_menu" src="/assets/fontend/images/logo_2_1.png"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="btn-cancel"><img src="/assets/fontend/images/simple-remove.png"></div>
                            <div class="clear"></div>
                            <ul class="nav navbar-nav">
                                <li class="item-menu"><a class="link-menu" href="/">Trang chủ</a></li>
                                <li class="dropdown item-menu ">
                                    <a href="#" class="dropdown-toggle link-menu " data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Chuyển tiền</a>
                                    <ul class="dropdown-menu">
                                        <a href="/rut-tien">
                                            <li><p>Rút tiền</p></li>
                                        </a>
                                        <a href="/nop-tien">
                                            <li><p>Nộp tiền</p></li>
                                        </a>
                                    </ul>
                                </li>
                                <li class="item-menu" id="btn-modal" data-toggle="modal" data-target="#myModal"><a
                                        class="link-menu" href="#">Đối tác</a></li>
                                <li class="item-menu active_menu"><a class="link-menu atv" href="/hoi-dap">Hỏi đáp</a></li>
                                <li class="dropdown item-menu ">
                                    <a href="#" class="dropdown-toggle link-menu " data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Tin tức</a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($categories as $k => $category){
                                            ?>
                                            <a href="/<?= $category->slug ?>">
                                                <li><p><?= $category->name ?></p></li>
                                            </a>
                                        <?php } ?>

                                    </ul>
                                </li>
                                <li class="item-menu"><a class="link-menu  " href="/lien-he">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h1>Hỏi đáp</h1>
            </div>
            <div class="col-md-6 col-md-6 col-xs-6">
                <div class="bg-img bg-img-question">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-question">
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>1</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Gutina có giao dịch tại nhà không?</h4>
                    <label><p>C&oacute;, khi bạn chọn h&igrave;nh thức giao dịch tận nh&agrave;</p></label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>2</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Trung bình bao lâu thì giao dịch xong?</h4>
                    <label><p>&quot;Trong trường hợp, qu&yacute; kh&aacute;ch h&agrave;ng giao dịch tại văn ph&ograve;ng
                            của Gutina, 7s sau tiền nổi trong t&agrave;i khoản ngay lập tức. Trường hợp, qu&yacute; kh&aacute;ch
                            y&ecirc;u cầu nh&acirc;n vi&ecirc;n giao dịch đến tận nơi, thời gian tiền nổi trong t&agrave;i
                            khoản t&iacute;nh từ l&uacute;c nh&acirc;n vi&ecirc;n giao dịch tại nh&agrave; bạn&quot;</p>
                    </label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>3</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Gutina có làm việc thứ Bảy, Chủ Nhật không?</h4>
                    <label><p>Gutina phục vụ 24/7. Cả trong v&agrave; ngo&agrave;i giờ h&agrave;nh ch&iacute;nh, thứ 7,
                            CN. Bất kể thời tiết mưa b&atilde;o, nắng n&ocirc;i hay ng&agrave;y lễ. Chỉ cần bạn y&ecirc;u
                            cầu, Gutina sẽ c&oacute; mặt, ho&agrave;n thiện giao dịch v&agrave; tiền nổi ngay trong t&agrave;i
                            khoản sau 7s.</p></label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>4</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Gutina có những hình thức giao dịch nào?</h4>
                    <label><p>Gutina cung cấp 3 h&igrave;nh thức giao dịch:</p>

                        <ul>
                            <li>Tiền mặt - Chuyển khoản: Gutina nhận tiền mặt rồi chuyển khoản</li>
                            <li>Chuyển khoản - Tiền mặt: Gutina nhận tiền rồi giao tiền mặt</li>
                            <li>Tiền mặt - Tiền mặt: Gutina nhận v&agrave; giao tiền mặt</li>
                        </ul>
                    </label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>5</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Làm sao để giao dịch với Gutina?</h4>
                    <label><p>Gutina c&oacute; 2 h&igrave;nh thức giao dịch:</p>

                        <ul>
                            <li>Giao dịch tại văn ph&ograve;ng: Kh&aacute;ch h&agrave;ng sẽ chỉ cần thanh to&aacute;n ph&iacute;
                                dịch vụ chuyển tiền của Gutina
                            </li>
                            <li>Giao dịch tận nh&agrave;: Ngo&agrave;i mức ph&iacute; giao dịch chuyển tiền, kh&aacute;ch
                                h&agrave;ng sẽ thanh to&aacute;n th&ecirc;m ph&iacute; giao dịch tại nh&agrave; t&ugrave;y
                                thuộc v&agrave;o từng khu vực&quot;
                            </li>
                        </ul>
                    </label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>6</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Sử dụng Gutina có an toàn, bảo mật thông tin không?</h4>
                    <label><p>C&oacute;, nh&acirc;n vi&ecirc;n nhận tiền phải viết bi&ecirc;n lai v&agrave; đ&oacute;ng
                            dấu đỏ.</p></label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>7</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Gutina giao dịch được những ngân hàng nào?</h4>
                    <label><p>40 ng&acirc;n h&agrave;ng tr&ecirc;n to&agrave;n quốc: Techcombank, Sacombank,
                            Vietcombank, Viettinbank, MB bank, BIDV, Agribank,VP Bank,TP Bank,...</p></label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>8</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Gutina có chuyển tiền ra nước ngoài được không?</h4>
                    <label><p>Hiện tại, Gutina chỉ chuyển tiền, nộp tiền v&agrave;o t&agrave;i khoản trong nước.</p>
                    </label>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-xs-1 col-sm-1">
                <h3>9</h3>
            </div>
            <div class="col-md-11 col-xs-11 col-sm-11">
                <div>
                    <h4>Giao dịch có chắc chắn hoàn thành không?</h4>
                    <label><p>Mọi giao dịch đều c&oacute; bi&ecirc;n lai v&agrave; dấu đỏ. Kh&aacute;ch h&agrave;ng giữ
                            1 li&ecirc;n v&agrave; nh&acirc;n vi&ecirc;n giao dịch giữ một li&ecirc;n. Nh&acirc;n vi&ecirc;n
                            giao dịch chỉ ra về khi người thụ hưởng đ&atilde; nhận được tiền. Vậy n&ecirc;n, qu&yacute; kh&aacute;ch
                            ho&agrave;n to&agrave;n c&oacute; thể y&ecirc;n t&acirc;m !</p></label>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-submit">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <h4>Gửi đến Gutina câu hỏi của bạn</h4>
                <form action="" method="post">
                    <textarea placeholder="Nhập câu hỏi" required name="content1"></textarea>
                    <button type="submit">Gửi</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><img
                            src="/assets/fontend/images/simple-remove.png"></button>
                <h4 class="modal-title" id="myModalLabel">Trở thành đối tác của Gutina</h4>
                <label>Vui lòng điền thông tin đầy đủ và chính xác để Gutina có thể liên hệ sớm đến bạn sớm nhất</label>
                <form action="/doi-tac" method="post">
                    <select class="input-contract" name="rank_salary" id="rank_salary">
                        <option><p>Khoảng thu nhập: Trên 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 10 triệu - 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 5 triệu - 9 triệu</p></option>
                        <option><p>Khoảng thu nhập: 3 triệu - 5 triệu</p></option>
                        <option><p>Khoảng thu nhập: Dưới 3 triệu</p></option>
                    </select>
                    <span class="glyphicon glyphicon-menu-down icon-down" aria-hidden="true"></span>
                    <input type="text" name="address" id="address" class="input-contract location" required
                           placeholder="Khu vực muốn hợp tác">
                    <input type="text" name="name" id="name" class="input-contract full-name" required placeholder="Họ và tên">
                    <input type="text" name="phone" id="phone" class="input-contract phone" required placeholder="Số điện thoại">
                    <input type="email" name="email" id="email" class="input-contract email" required placeholder="Email">
                    <button type="button" class="btn-contract" name="submit" id="submit">Đăng ký ngay</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <a href="tel:+84962014588" class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle"></div>
    </a>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="logo">
                <img src="/assets/fontend/images/logo_2_1.png">
                <h4><span class="txt-pc">Dịch vụ chuyển</span><span class="txt-mb">Chuyển</span> tiền theo mô hình
                    Uber<br> với đội ngũ nhân viên điều phối và giao dịch chuyên nghiệp, tận tâm.</h4>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_left">
                <div class="logo-1"><img src="/assets/fontend/images/pin-3.png"><span>Tầng M, Tòa B Hoàng Huy, 275 Nguyễn Trãi, Thanh Xuân, Hà Nội</span>
                </div>
                <div class="logo-1"><img src="/assets/fontend/images/phone-2.png"><span><a class="rm-decoration"
                                                                           href="tel:+84962014588">0962.014.588</a> | <a
                            class="rm-decoration" href="tel:+84947060588">0947.060.588</a></span></div>
                <div class="logo-1"><img src="/assets/fontend/images/email-83.png"><span>gutina.247@gmail.com</span></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="map">
                <p class="txt-map">Chỉ đường trên google map</p>
                <div id="googleMap" style="width:100%;height:150px;"></div>

            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_right">
                <form method="post" action="/email">
                    <input type="hidden" name="_token" value="ZH8W1M234DfmmLqZBl9deRJInyYnvy9V2r7pz3tn">
                    <h4>Đăng ký nhận ưu đãi</h4>
                    <div class="btn-email">
                        <input type="email" name="email" required placeholder="Email của bạn">
                        <button type="submit" class="btn btn-success">Gửi</button>
                    </div>
                </form>
                <a href="https://www.facebook.com/Gutina247/"><img src="/assets/fontend/images/Facebook_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Google_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Youtube_Icon.png"></a>
            </div>
        </div>
    </div>
</div>
</body>
<?php include "../Views/fontend/js.php"; ?>
</html>