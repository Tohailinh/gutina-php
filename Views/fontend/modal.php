<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><img
                        src="/assets/fontend/images/simple-remove.png"></button>
                <h4 class="modal-title" id="myModalLabel">Trở thành đối tác của Gutina</h4>
                <label>Vui lòng điền thông tin đầy đủ và chính xác để Gutina có thể liên hệ sớm đến bạn sớm nhất</label>
                <form action="/doi-tac" method="post">
                    <select class="input-contract" name="rank_salary" id="rank_salary">
                        <option><p>Khoảng thu nhập: Trên 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 10 triệu - 15 triệu</p></option>
                        <option><p>Khoảng thu nhập: 5 triệu - 9 triệu</p></option>
                        <option><p>Khoảng thu nhập: 3 triệu - 5 triệu</p></option>
                        <option><p>Khoảng thu nhập: Dưới 3 triệu</p></option>
                    </select>
                    <span class="glyphicon glyphicon-menu-down icon-down" aria-hidden="true"></span>
                    <input type="text" name="address" id="address" class="input-contract location" required
                           placeholder="Khu vực muốn hợp tác">
                    <input type="text" name="name" id="name" class="input-contract full-name" required placeholder="Họ và tên">
                    <input type="text" name="phone" id="phone" class="input-contract phone" required placeholder="Số điện thoại">
                    <input type="email" name="email" id="email" class="input-contract email" required placeholder="Email">
                    <button type="button" class="btn-contract" name="submit" id="submit">Đăng ký ngay</button>
                </form>
            </div>
        </div>
    </div>
</div>