<?php
use App\Models\Category;
$where = 1;
$where .= " AND parent_id = 0";
$categories = Category::readAll('category', $where);
?>

<div id="menu" class="row">
    <div class="menu_top">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button id="btn_menu" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/"><img class="logo_menu" src="/assets/fontend/images/logo_2_1.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="btn-cancel"><img src="/assets/fontend/images/simple-remove.png"></div>
                    <div class="clear"></div>
                    <ul class="nav navbar-nav">
                        <li class="menu-home item-menu"><a class="link-menu" href="/">Trang chủ</a></li>
                        <li class="item-menu" id="btn-modal" data-toggle="modal" data-target="#myModal"><a
                                class="link-menu" href="#">Đối tác</a></li>
                        <li class="item-menu"><a class="link-menu" href="/hoi-dap">Hỏi đáp</a>
                        </li>
                        <li class="dropdown item-menu">
                            <a href="#" class="dropdown-toggle link-menu" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Tin tức</a>
                            <ul class="dropdown-menu">
                                <?php
                                    foreach ($categories as $k => $category){
                                ?>
                                    <a href="/<?= $category->slug ?>">
                                        <li><p><?= $category->name ?></p></li>
                                    </a>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="item-menu"><a class="link-menu" href="/lien-he">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>