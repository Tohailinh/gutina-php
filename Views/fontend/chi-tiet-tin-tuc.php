<?php
use App\Models\News;
?>

<!DOCTYPE html>
<html>
<head>
    <title>Vietcombank dẫn đầu danh sách các ngân hàng uy tín tại Việt Nam</title>
    <meta name="description"
          content="Vietcombank dẫn đầu danh sách các ngân hàng uy tín tại Việt Nam. Ngân hàng Vietcombank là một trong những ngân hàng có số lượng khách hàng lớn nhất hiện nay. Với các dịch vụ đáp ứng được nhu cầu của người dùng và quá trình chăm sóc khách hàng tận tình đã giúp ngân hàng này lọt TOP các ngân hàng được nhiều người sử dụng hiện nay.">
    <meta property="og:url"
          content="https://gutina.vn/tin-tuc/vietcombank-dan-dau-danh-sach-cac-ngan-hang-uy-tin-tai-viet-nam">
    <meta property="og:title" content="Vietcombank dẫn đầu danh sách các ngân hàng uy tín tại Việt Nam">
    <meta property="og:description"
          content="Vietcombank dẫn đầu danh sách các ngân hàng uy tín tại Việt Nam. Ngân hàng Vietcombank là một trong những ngân hàng có số lượng khách h...">
    <meta property="og:image"
          content="images/14-17-17-2018-12-18-vietcombank-dan-dau-cac-ngan-hang-uy-tin-tai-viet-nam.jpg">
    <meta charset="utf-8">
    <meta name="keywords" content="Vietcombank">
    <link rel="icon" href="images/icon.png" sizes="32x32">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "../Views/fontend/css.php"; ?>
    <!-- Google Tag Manager -->
</head>
<body>
<!-- End Google Tag Manager (noscript) -->
<div id="top">
    <div id="content" class="container">
        <?php include "../Views/fontend/menu-1.php"; ?>
    </div>
</div>

<div class="clear"></div>
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h1>Tin tức</h1>
            </div>
            <div class="col-md-6 col-md-6 col-xs-6">
                <div class="bg-img bg-news">
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="content">
                    <h4 class="title-news"><?= $news_detail->name ?></h4>
                    <div class="details-content">
                        <?= $news_detail->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-news">
    <div class="container">
        <div class="fb-comments"
             data-href="https://gutina.vn/tin-tuc/vietcombank-dan-dau-danh-sach-cac-ngan-hang-uy-tin-tai-viet-nam"
             data-numposts="5"></div>
        <hr>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="new-other">Bài viết khác</h3>
            </div>
        </div>
        <?php
        foreach ($news_random as $new) {
            ?>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="row-news">
                    <div class="img-news">
                        <a href="/tin-tuc/<?= $antiXss->xss_clean($new['slug']) ?>">
                            <img src="/uploads/news/<?= $antiXss->xss_clean($new['avatar']) ?>" alt="<?= $antiXss->xss_clean($new['name']) ?>">
                        </a>
                    </div>
                    <label><?= $new['created_at'] ?></label>
                    <h4><a href="/tin-tuc/<?= $antiXss->xss_clean($new['slug']) ?>"><?= $antiXss->xss_clean($new['name']) ?></a></h4>
                    <span class="desc">
                    <a href="/tin-tuc/<?= $antiXss->xss_clean($new['slug']) ?>">
                        <?php
                            $description = News::trimmedTitle($new['description']);
                            echo $antiXss->xss_clean($description);
                        ?>
                    </a>
                </span>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div class="line">
    <hr>
</div>
<?php include "../Views/fontend/modal.php"; ?>
<div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <a href="tel:+84962014588" class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle"></div>
    </a>
</div>
<div class="footer">
    <div class="container">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="logo">
                <img src="/assets/fontend/images/logo_2_1.png">
                <h4><span class="txt-pc">Dịch vụ <a class="rm-decoration" href="http://g-pay.vn/chuyen-tien/">chuyển tiền nhanh</a></span><span
                            class="txt-mb"><a class="rm-decoration"
                                              href="http://g-pay.vn/chuyen-tien/">Chuyển tiền nhanh</a></span> theo mô
                    hình
                    Uber<br> với đội ngũ nhân viên điều phối và giao dịch chuyên nghiệp, tận tâm.</h4>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_left">
                <div class="logo-1"><img src="/assets/fontend/images/pin-3.png"><span>Tầng M, Tòa B Hoàng Huy, 275 Nguyễn Trãi, Thanh Xuân, Hà Nội</span>
                </div>
                <div class="logo-1"><img src="/assets/fontend/images/phone-2.png"><span><a class="rm-decoration"
                                                                                           href="tel:+84962014588">0962.014.588</a> | <a
                                class="rm-decoration" href="tel:+84947060588">0947.060.588</a></span></div>
                <div class="logo-1"><img src="/assets/fontend/images/email-83.png"><span>gutina.247@gmail.com</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="map">
                <p class="txt-map">Chỉ đường trên google map</p>
                <div id="googleMap" style="width:100%;height:150px;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.986055165282!2d105.80538691540202!3d20.993196094366265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbf6af3c44f%3A0xed2a3283251a5bf7!2zMjc1IE5ndXnhu4VuIFRyw6NpLCBUaGFuaCBYdcOibiBUcnVuZywgVGhhbmggWHXDom4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1546402222762"
                             frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer_right">
                <form method="post" action="https://gutina.vn/email">
                    <input type="hidden" name="_token" value="VtrNt27j1ceOjj5oqbbzB1yYraZ35O4O5ZUiGUd6">
                    <h4>Đăng ký nhận ưu đãi</h4>
                    <div class="btn-email">
                        <input type="email" name="email" required placeholder="Email của bạn">
                        <button type="submit" class="btn btn-success">Gửi</button>
                    </div>
                </form>
                <a href="https://www.facebook.com/Gutina247/"><img src="/assets/fontend/images/Facebook_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Google_Icon.png"></a>
                <a href="#"><img src="/assets/fontend/images/Youtube_Icon.png"></a>
            </div>
        </div>
    </div>
</div>
</body>

<?php include "../Views/fontend/js.php"; ?>
</html>